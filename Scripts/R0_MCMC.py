import numpy as np
import pandas as pd
from scipy.integrate import solve_ivp
import time
from scipy.stats import nbinom
import pymc as pm
from scipy.interpolate import interp1d
import pytensor.tensor as pt
import pickle

print(f"Running on PyMC v{pm.__version__}")


# path to directory
path = ".../COVIDNC"

# Read vaccination data
dfvac = pd.read_csv(path + "/Data/dfvac.csv", parse_dates=["date"])
dfvac = dfvac.drop(["cumultot", "prct"], axis=1)

dfhosp = pd.read_csv(path + "/Data/dfobs.csv", parse_dates=["date"])

# Define key dates
date_start = pd.to_datetime(dfvac.date[0])
date_intro = pd.to_datetime("2021-08-10")
date_lock = pd.to_datetime("2021-09-07")
date_partial_lift = pd.to_datetime("2021-10-11")
date_sim_end = pd.to_datetime(date_partial_lift)

# Calculate phase lengths
l1 = (date_intro - date_start).days
l2 = (date_lock - date_intro).days
l3 = (date_partial_lift - date_lock).days
ls = [l1, l2, l3]

# Set up date and time
dates = pd.date_range(date_start, date_sim_end, inclusive="left")
sim_len = len(dates)
sample_times = np.arange(sim_len)

# Set up observations
obs_start = pd.to_datetime("2021-09-06")
dfobs = dfobs.loc[(dfobs.date >= obs_start) & (dfobs.date < date_sim_end)].copy()
inds_obs = np.where(dates.isin(dfobs.date))[0]
hosp_obs = dfobs.hosp.values

# Process vaccination data and create an interpolation function
matvac = np.transpose(dfvac.values[:sim_len, 1:])
vacFunc = interp1d(sample_times, matvac, bounds_error=False, fill_value="extrapolate")

# Set initial conditions
n_a = 6  # age groups
n_s = 38  # statuts
y0 = np.zeros([n_a, n_s])
y0[:, 0] = [48039, 16501, 134761, 44684, 16639, 10783]  # S0
y0 = y0.flatten()

hosp_probs = [0.002,
              0.001,
              0.006366023794823,
              0.038815175342405,
              0.088984674679969,
              0.218271736807938]


# Assume delta severity
hosp_probs = np.array([1.5, 1.5, 1.5, 1.5, 1.5, 1.00]) * np.array(hosp_probs)

# Define susceptibility factors
susceps = np.array([(1 - 0.5), (1 - 0.25), 1, 1, 1, 1])

# Define protection factors for infection and severity
inf_vac = 0.40
sus_vac1 = 0.33
sus_vac2 = 0.85
sev_vac1 = 0.85
sev_vac2 = 0.95
protecs = np.array([inf_vac, sus_vac1, sev_vac1, sus_vac2, sev_vac2])

# Set initial value for infection in age group 16.49 and create an interpolation function
i0 = 1
infs = np.zeros((n_a, sim_len))
infs[3, l1] = i0
infFunc = interp1d(sample_times, infs, bounds_error=False, fill_value="extrapolate")


def SEIR(t, y, pHosps, susceps, protecs, infFunc, vacFunc, R0Func):

    """
    SEIHR

    input: t, y, pHosps, susceps, protecs, vacFuncs, infFuncs, (R0, a1)

    output: S, E1, E2, Imild, Ihosp, H, R, Icumul, Hcumul (.,V0,V1,V2), SV1cumul, SV2cumul

    """


    # Population of New Caledonia
    N = 271407

    # Incubation, Presymptomatic, Symptomatic, Hospital stay period
    tr1, tr2, tr3, tr_Hout = 4, 1, 3, 8
    r1, r2, r3, r_Hout = 1 / tr1, 1 / tr2, 1 / tr3, 1 / tr_Hout

    # Total infectious period
    tInf = tr3 + tr2

    # Transmission rate
    Rj = R0Func(t) / tInf

    # Set vaccine protection parameters
    infVac, suscepVac1, sevVac1, suscepVac2, sevVac2 = protecs

    tVac1, tVac2 = 1/14, 1/14

    dydt = np.zeros(len(y))
    age_indices = n_s * np.arange(n_a)

    # Infectious totals across age groups according to vaccine statuts
    E2tot, E2V0tot, E2V1tot, E2V2tot = np.sum([y[i + age_indices] for i in [2, 11, 20, 29]], axis=1)
    Imildtot, ImildV0tot, ImildV1tot, ImildV2tot = np.sum([y[i + age_indices] for i in [3, 12, 21, 30]], axis=1)
    Ihosptot, IhospV0tot, IhospV1tot, IhospV2tot = np.sum([y[i + age_indices] for i in [4, 13, 22, 31]], axis=1)

    # Total number of infectious individuals
    Inum = ((E2tot + Imildtot + Ihosptot) +
            (E2V0tot + ImildV0tot + IhospV0tot) +
            (1-infVac) * (E2V1tot + ImildV1tot + IhospV1tot) +
            (1-infVac) * (E2V2tot + ImildV2tot + IhospV2tot))

    # FOI
    Itot = Inum / N

    # Just retrieve non cumulative variables (7/9)
    var_idx = np.array([i + np.arange(7) for i in np.arange(0, 28, 9)]).ravel()

    S, E1, E2, Imild, Ihosp, H, R, \
    SV0, E1V0, E2V0, ImildV0, IhospV0, HV0, RV0,\
    SV1, E1V1, E2V1, ImildV1, IhospV1, HV1, RV1, \
    SV2, E1V2, E2V2, ImildV2, IhospV2, HV2, RV2 = y[np.array([i + age_indices for i in var_idx])]

    tot_non_vac = S + E1 + E2 + Imild + Ihosp + H + R

    dydt[0 + age_indices] = -(Rj * susceps * S * Itot) - infFunc(t) - (vacFunc(t) * S / tot_non_vac)
    dydt[1 + age_indices] = (Rj * susceps * S * Itot) - (r1 * E1) - (vacFunc(t) * E1 / tot_non_vac)
    dydt[2 + age_indices] = infFunc(t) + (r1 * E1) - (r2 * E2) - (vacFunc(t) * E2 / tot_non_vac)
    dydt[3 + age_indices] = ((1 - pHosps) * r2 * E2) - (r3 * Imild) - (vacFunc(t) * Imild / tot_non_vac)
    dydt[4 + age_indices] = (pHosps * r2 * E2) - (r3 * Ihosp) - (vacFunc(t) * Ihosp / tot_non_vac)
    dydt[5 + age_indices] = (r3 * Ihosp) - (r_Hout * H) - (vacFunc(t) * H / tot_non_vac)
    dydt[6 + age_indices] = (r3 * Imild) + (r_Hout * H) - (vacFunc(t) * R / tot_non_vac)
    dydt[7 + age_indices] = (r2 * E2)
    dydt[8 + age_indices] = (r3 * Ihosp)

    dydt[9 + age_indices] = (vacFunc(t) * S / tot_non_vac) - (Rj * susceps * SV0 * Itot) - (tVac1 * SV0)
    dydt[10 + age_indices] = ((vacFunc(t) * E1 / tot_non_vac) + (Rj * susceps * SV0 * Itot)
                                         - (r1 * E1V0) - (tVac1 * E1V0))
    dydt[11 + age_indices] = (vacFunc(t) * E2 / tot_non_vac) + (r1 * E1V0) - (r2 * E2V0) - (tVac1 * E2V0)
    dydt[12 + age_indices] = ((vacFunc(t) * Imild / tot_non_vac) + ((1 - pHosps) * r2 * E2V0)
                                         - (r3 * ImildV0) - (tVac1 * ImildV0))
    dydt[13 + age_indices] = ((vacFunc(t) * Ihosp / tot_non_vac) + (pHosps * r2 * E2V0)
                                         - (r3 * IhospV0) - (tVac1 * IhospV0))
    dydt[14 + age_indices] = (vacFunc(t) * H / tot_non_vac) + (r3 * IhospV0) - (r_Hout * HV0) - (tVac1 * HV0)
    dydt[15 + age_indices] = (vacFunc(t) * R / tot_non_vac) + (r3 * ImildV0) + (r_Hout * HV0) - (tVac1 * RV0)
    dydt[16 + age_indices] = (r2 * E2V0)
    dydt[17 + age_indices] = (r3 * IhospV0)

    dydt[18 + age_indices] = (tVac1 * SV0) - (Rj * susceps * (1 - suscepVac1) * SV1 * Itot) - (tVac2 * SV1)
    dydt[19 + age_indices] = ((tVac1 * E1V0) + (Rj * susceps * (1 - suscepVac1) * SV1 * Itot)
                                         - (r1 * E1V1) - (tVac2 * E1V1))
    dydt[20 + age_indices] = (tVac1 * E2V0) + (r1 * E1V1) - (r2 * E2V1) - (tVac2 * E2V1)
    dydt[21 + age_indices] = ((tVac1 * ImildV0) + ((1 - (pHosps * (1 - sevVac1))) * r2 * E2V1)
                                         - (r3 * ImildV1) - (tVac2 * ImildV1))
    dydt[22 + age_indices] = ((tVac1 * IhospV0) + ((pHosps * (1 - sevVac1)) * r2 * E2V1)
                                         - (r3 * IhospV1) - (tVac2 * IhospV1))
    dydt[23 + age_indices] = (tVac1 * HV0) + (r3 * IhospV1) - (r_Hout * HV1) - (tVac2 * HV1)
    dydt[24 + age_indices] = (tVac1 * RV0) + (r3 * ImildV1) + (r_Hout * HV1) - (tVac2 * RV1)
    dydt[25 + age_indices] = (r2 * E2V1)
    dydt[26 + age_indices] = (r3 * IhospV1)

    dydt[27 + age_indices] = (tVac2 * SV1) - (Rj * susceps * (1 - suscepVac2) * SV2 * Itot)
    dydt[28 + age_indices] = (tVac2 * E1V1) + (Rj * susceps * (1 - suscepVac2) * SV2 * Itot) - (r1 * E1V2)
    dydt[29 + age_indices] = (tVac2 * E2V1) + (r1 * E1V2) - (r2 * E2V2)
    dydt[30 + age_indices] = ((tVac2 * ImildV1) + ((1 - (pHosps * (1 - sevVac2))) * r2 * E2V2)
                                         - (r3 * ImildV2))
    dydt[31 + age_indices] = (tVac2 * IhospV1) + ((pHosps * (1 - sevVac2)) * r2 * E2V2) - (r3 * IhospV2)
    dydt[32 + age_indices] = (tVac2 * HV1) + (r3 * IhospV2) - (r_Hout * HV2)
    dydt[33 + age_indices] = (tVac2 * RV1) + (r3 * ImildV2) + (r_Hout * HV2)
    dydt[34 + age_indices] = (r2 * E2V2)
    dydt[35 + age_indices] = (r3 * IhospV2)

    dydt[36 + age_indices] = ((vacFunc(t) * S / tot_non_vac)
                                         + (vacFunc(t) * E1 / tot_non_vac)
                                         + (vacFunc(t) * E2 / tot_non_vac)
                                         + (vacFunc(t) * Imild / tot_non_vac)
                                         + (vacFunc(t) * Ihosp / tot_non_vac)
                                         + (vacFunc(t) * H / tot_non_vac)
                                         + (vacFunc(t) * R / tot_non_vac))

    dydt[37 + age_indices] = ((tVac2 * SV1)
                                         + (tVac2 * E1V1)
                                         + (tVac2 * E2V1)
                                         + (tVac2 * ImildV1)
                                         + (tVac2 * IhospV1)
                                         + (tVac2 * HV1)
                                         + (tVac2 * RV1))

    # print(np.sum(dydt, axis=0))
    return dydt


def switch_eta(t, t1, eta, nu, k=1):
    switch = eta / (1 + np.exp(-k * (t - t1 - nu)))
    return switch


def my_model(R0, eta, nu):

    Rnull = 0
    tlock = l1 + l2
    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R0, (l2+l3))))
    switch = switch_eta(sample_times, t1=tlock,  eta=eta, nu=nu)
    Reffs = R0s * (1 - switch)

    # Interpolate the effective reproduction number (Reff) as a function of time
    R0Func = interp1d(sample_times, Reffs, bounds_error=False, fill_value="extrapolate")

    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, infFunc, vacFunc, R0Func),
                    t_eval=sample_times,
                    method="LSODA")

    age_indices = n_s * np.arange(n_a)
    Hcumulflat = np.sum(sol.y[[i + j for i in [8, 17, 26, 35] for j in age_indices], :], axis=0)
    iHosp = Hcumulflat[1:] - Hcumulflat[:-1]
    iHosp_simul = np.insert(iHosp, 0, Hcumulflat[0])
    iHosp_simul = iHosp_simul[inds_obs]

    return iHosp_simul


def my_loglike(theta, hosp_obs):

    R0, eta, nu, alpha_hosp = theta
    # print(f"R0: {R0}")
    # print(f"eta: {eta}")
    # print(f"nu: {nu}")

    iHosp_simul = my_model(R0=R0, eta=eta, nu=nu)
    n_hosp = 1/alpha_hosp

    mu_hosp = iHosp_simul
    p_hosp = mu_hosp / (mu_hosp + n_hosp)
    lls = nbinom.logpmf(hosp_obs, n_hosp, 1.0 - p_hosp)
    ll = np.sum(lls)

    return ll


# define a pytensor Op for our likelihood function
class LogLike(pt.Op):

    """
    Specify what type of object will be passed and returned to the Op when it is
    called. In our case we will be passing it a vector of values (the parameters
    that define our model) and returning a single "scalar" value (the
    log-likelihood)
    """

    itypes = [pt.dvector]  # expects a vector of parameter values when called
    otypes = [pt.dscalar]  # outputs a single scalar value (the log likelihood)

    def __init__(self, loglike, data):

        """
        Initialise the Op with various things that our log-likelihood function
        requires. Below are the things that are needed in this particular
        example.

        Parameters
        ----------
        loglike:
            The log-likelihood (or whatever) function we've defined
        data:
            The "observed" data that our log-likelihood function takes in
        x:
            The dependent variable (aka 'x') that our model requires
        sigma:
            The noise standard deviation that our function requires.
        """

        # add inputs as class attributes
        self.likelihood = loglike
        self.data = data

    def perform(self, node, inputs, outputs):
        # the method that is used when calling the Op
        (theta,) = inputs  # this will contain my variables

        # call the log-likelihood function
        logl = self.likelihood(theta, self.data)

        outputs[0][0] = np.array(logl)  # output the log-likelihood


# Run
ndraws = 3000  # number of draws from the distribution
nburn = 1000  # number of "burn-in points" (which we'll discard)
# create our Op
logl = LogLike(my_loglike, hosp_obs)

# use PyMC to sampler from log-likelihood
start = time.perf_counter()
with pm.Model():

    # R0 = pm.Bound(pm.Normal, lower=1, upper=12)("R0", 7, 2)
    normal_dist = pm.Normal.dist(mu=6.00, sigma=2.0)
    R0 = pm.Truncated("R0", normal_dist, lower=1, upper=12)
    beta = pm.Deterministic("beta",  (R0 / 4))

    eta = pm.Beta("eta", alpha=8, beta=2)
    exp_dist = pm.Exponential.dist(lam=1/5)
    nu = pm.Truncated("nu", exp_dist, lower=3, upper=15)
    # k = pm.Uniform("k", lower=0.5, upper=1.5)

    alpha_hosp = pm.Exponential("alpha_hosp", lam=5)
    # convert thetas to a tensor vector
    theta = pt.as_tensor_variable([R0, eta, nu, alpha_hosp])

    # use a DensityDist (use a lamdba function to "call" the Op)
    pm.Potential('likelihood', logl(theta))

    idata = pm.sample(ndraws, tune=nburn, chains=4, random_seed=123, return_inferencedata=True, cores=4)

print(f"Completed Execution in {time.perf_counter() - start} seconds")

with open(path + "/Production/idata_MCMC", "wb") as f1:
    pickle.dump(idata, f1)
