import numpy as np
import pandas as pd
from scipy.stats import expon
from multiprocessing import Pool
import pickle
import sys
from time import perf_counter

# path to directory
path = ".../COVIDNC"
sys.path.append(path + "/Scripts")

from simICU2023 import *

dfobs = pd.read_csv(path + "/Data/dfobs.csv", parse_dates=["date"])
dates_obs = dfobs.date
hosps = dfobs.hosp.values
icus = dfobs.icu.values


# Retrieve simulations
with open(path + "/Production/scenarios.pkl", "rb") as f:
    dscenarios = pickle.load(f)

dates = dscenarios["date"]

tauHosp_ICU = 0.5

# daily pICU
pICU = estimate_pICU(hosps, icus, tauHosp_ICU=tauHosp_ICU, window=1)
pICU[pICU < 0] = 0
mu_tauICU_R = [10.56]
cpoints = []

# fixed pICUs
mu_pICU = np.sum(icus) / np.sum(hosps)
pICU2 = np.full_like(pICU, fill_value=mu_pICU, dtype=float)


def fsim(i, hosps_sims):

    hosps_sim = hosps_sims[i, :]

    icu_sim = simulate_iICU(hosps_sim, pICU,
                            tauHosp_ICU=tauHosp_ICU, draw=False)
    icu_occ_sim = simulate_inICU(iICU=icu_sim, spoints_tauICU_R=cpoints,
                                 mu_tauICU_R=mu_tauICU_R, window=1,
                                 draw=False, debug=False)

    return icu_sim, icu_occ_sim


def fsim_alt(i, hosps_sims):

    hosps_sim = hosps_sims[i, :]

    icu_sim = simulate_iICU(hosps_sim, pICU2,
                            tauHosp_ICU=tauHosp_ICU, draw=False)
    icu_occ_sim = simulate_inICU(iICU=icu_sim, spoints_tauICU_R=cpoints,
                                 mu_tauICU_R=mu_tauICU_R, window=1,
                                 draw=False, debug=False)

    return icu_sim, icu_occ_sim


dICU = {}
for sim in ["hosps_base", "hosps_vac_partial", "hosps_nolock",
            "hosps_sooner"]:

    print(f"{sim}...")
    hosps_sims = dscenarios[sim]
    hosps_sims = hosps_sims[:, dates.isin(dates_obs)]

    tic = perf_counter()
    with Pool(16) as p:
        results = p.starmap(fsim, [(i, hosps_sims) for i in range(hosps_sims.shape[0])])

    toc = perf_counter()
    print(f"{sim}: done in {(toc - tic)/60:.1f} minutes !")

    icu_sims, icu_occ_sims = zip(*results)

    icu_sims = np.array(icu_sims)
    icu_occ_sims = np.array(icu_occ_sims)

    if sim == "hosps_vac_partial":
        suffixe = sim.split("_")[1] + "_" + sim.split("_")[2]
        dICU[f"icu_{suffixe}"] = icu_sims
        dICU[f"occ_icu_{suffixe}"] = icu_occ_sims
    else:
        suffixe = sim.split("_")[1]
        dICU[f"icu_{suffixe}"] = icu_sims
        dICU[f"occ_icu_{suffixe}"] = icu_occ_sims


for sim in ["hosps_base", "hosps_vac_partial", "hosps_nolock",
            "hosps_sooner"]:

    print(f"{sim}...")
    hosps_sims = dscenarios[sim]
    hosps_sims = hosps_sims[:, dates.isin(dates_obs)]

    tic = perf_counter()
    with Pool(16) as p:
        results = p.starmap(fsim_alt, [(i, hosps_sims) for i in range(hosps_sims.shape[0])])

    toc = perf_counter()
    print(f"{sim} Alt: done in {(toc - tic) / 60:.1f} minutes! ")

    icu_sims_alt, icu_occ_sims_alt = zip(*results)

    icu_sims_alt = np.array(icu_sims_alt)
    icu_occ_sims_alt = np.array(icu_occ_sims_alt)

    if sim == "hosps_vac_partial":
        suffixe = sim.split("_")[1] + "_" + sim.split("_")[2]
        dICU[f"icu_{suffixe}_alt"] = icu_sims_alt
        dICU[f"occ_icu_{suffixe}_alt"] = icu_occ_sims_alt
    else:
        suffixe = sim.split("_")[1]
        dICU[f"icu_{suffixe}_alt"] = icu_sims_alt
        dICU[f"occ_icu_{suffixe}_alt"] = icu_occ_sims_alt


dICU["date_obs"] = dates_obs

# Regroup in one dictionnary
dsims = {}
for (k1, v1) in dscenarios.items():
    dsims[k1] = v1

for (k2, v2) in dICU.items():
    dsims[k2] = v2

with open(path + "/Production/dsims.pkl", "wb") as f:
    pickle.dump(dsims, f)
