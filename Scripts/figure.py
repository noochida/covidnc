import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import pickle
import locale
import sys

# Set the locale to the desired language, e.g., 'fr_FR' for French, 'es_ES' for Spanish, 'de_DE' for German
locale.setlocale(locale.LC_ALL, 'en_GB.utf8')

# path to directory
path = ".../COVIDNC"
sys.path.append(path + "/Scripts")

plt.style.use("classic")

# Outputs
with open(path + "/Production/dsims.pkl", "rb") as f:
    dsims = pickle.load(f)

dates_sim = dsims["date"]
date_obs_sim = dsims["date_obs"]

# base
infs = dsims["infs_base"]
minfs = np.mean(dsims["infs_base"], axis=0)

hosps = dsims["hosps_base"]
hosps = hosps[:, np.isin(dates_sim, date_obs_sim)]
qhosps = np.quantile(hosps, q=[0.025, 0.975], axis=0)
mhosps = np.mean(hosps, axis=0)

icu = dsims["icu_base"]
qicu = np.quantile(icu, q=[0.025, 0.975], axis=0)
micu = np.mean(icu, axis=0)

icu_alt = dsims["icu_base_alt"]
qicu_alt = np.quantile(icu_alt, q=[0.025, 0.975], axis=0)
micu_alt = np.mean(icu_alt, axis=0)

occ_icu = dsims["occ_icu_base"]
qocc_icu = np.quantile(occ_icu, q=[0.025, 0.975], axis=0)
mocc_icu = np.mean(occ_icu, axis=0)

occ_icu_alt = dsims["occ_icu_base_alt"]
qocc_icu_alt = np.quantile(occ_icu_alt, q=[0.025, 0.975], axis=0)
mocc_icu_alt = np.mean(occ_icu_alt, axis=0)

# Partial vaccination
infs_vac_partial = dsims["infs_vac_partial"]
minfs_vac_partial = np.mean(dsims["infs_vac_partial"], axis=0)

hosps_vac_partial = dsims["hosps_vac_partial"]
hosps_vac_partial = hosps_vac_partial[:, np.isin(dates_sim, date_obs_sim)]
qhosps_vac_partial = np.quantile(hosps_vac_partial, q=[0.025, 0.975], axis=0)
mhosps_vac_partial = np.mean(hosps_vac_partial, axis=0)

icu_vac_partial = dsims["icu_vac_partial"]
qicu_vac_partial = np.quantile(icu_vac_partial, q=[0.025, 0.975], axis=0)
micu_vac_partial = np.mean(icu_vac_partial, axis=0)

icu_vac_partial_alt = dsims["icu_vac_partial_alt"]
qicu_vac_partial_alt = np.quantile(icu_vac_partial_alt, q=[0.025, 0.975], axis=0)
micu_vac_partial_alt = np.mean(icu_vac_partial_alt, axis=0)

occ_icu_vac_partial = dsims["occ_icu_vac_partial"]
qocc_icu_vac_partial = np.quantile(occ_icu_vac_partial, q=[0.025, 0.975], axis=0)
mocc_icu_vac_partial = np.mean(occ_icu_vac_partial, axis=0)

occ_icu_vac_partial_alt = dsims["occ_icu_vac_partial_alt"]
qocc_icu_vac_partial_alt = np.quantile(occ_icu_vac_partial_alt, q=[0.025, 0.975], axis=0)
mocc_icu_vac_partial_alt = np.mean(occ_icu_vac_partial_alt, axis=0)

# No lockdwon
infs_nolock = dsims["infs_nolock"]
minfs_nolock = np.mean(dsims["infs_nolock"], axis=0)

hosps_nolock = dsims["hosps_nolock"]
hosps_nolock = hosps_nolock[:, np.isin(dates_sim, date_obs_sim)]
qhosps_nolock = np.quantile(hosps_nolock, q=[0.025, 0.975], axis=0)
mhosps_nolock = np.mean(hosps_nolock, axis=0)

icu_nolock = dsims["icu_nolock"]
qicu_nolock = np.quantile(icu_nolock, q=[0.025, 0.975], axis=0)
micu_nolock = np.mean(icu_nolock, axis=0)

icu_nolock_alt = dsims["icu_nolock_alt"]
qicu_nolock_alt = np.quantile(icu_nolock_alt, q=[0.025, 0.975], axis=0)
micu_nolock_alt = np.mean(icu_nolock_alt, axis=0)

occ_icu_nolock = dsims["occ_icu_nolock"]
qocc_icu_nolock = np.quantile(occ_icu_nolock, q=[0.025, 0.975], axis=0)
mocc_icu_nolock = np.mean(occ_icu_nolock, axis=0)

occ_icu_nolock_alt = dsims["occ_icu_nolock_alt"]
qocc_icu_nolock_alt = np.quantile(occ_icu_nolock_alt, q=[0.025, 0.975], axis=0)
mocc_icu_nolock_alt = np.mean(occ_icu_nolock_alt, axis=0)

# Sooner
infs_sooner = dsims["infs_sooner"]
minfs_sooner = np.mean(dsims["infs_sooner"], axis=0)

hosps_sooner = dsims["hosps_sooner"]
hosps_sooner = hosps_sooner[:, np.isin(dates_sim, date_obs_sim)]
qhosps_sooner = np.quantile(hosps_sooner, q=[0.025, 0.975], axis=0)
mhosps_sooner = np.mean(hosps_sooner, axis=0)

icu_sooner = dsims["icu_sooner"]
qicu_sooner = np.quantile(icu_sooner, q=[0.025, 0.975], axis=0)
micu_sooner = np.mean(icu_sooner, axis=0)

icu_sooner_alt = dsims["icu_sooner_alt"]
qicu_sooner_alt = np.quantile(icu_sooner_alt, q=[0.025, 0.975], axis=0)
micu_sooner_alt = np.mean(icu_sooner_alt, axis=0)

occ_icu_sooner = dsims["occ_icu_sooner"]
qocc_icu_sooner = np.quantile(occ_icu_sooner, q=[0.025, 0.975], axis=0)
mocc_icu_sooner = np.mean(occ_icu_sooner, axis=0)

occ_icu_sooner_alt = dsims["occ_icu_sooner_alt"]
qocc_icu_sooner_alt = np.quantile(occ_icu_sooner_alt, q=[0.025, 0.975], axis=0)
mocc_icu_sooner_alt = np.mean(occ_icu_sooner_alt, axis=0)

#Dataframes

dfobs = pd.read_csv(path + "/Data/dfobs.csv", parse_dates=["date"])
dfvacobs = pd.read_csv(path + "/Data/dfvac.csv", parse_dates=["date"])

##########
# Figure 1
##########

date_intro = pd.to_datetime("2021-08-10")
date_lock = pd.to_datetime("2021-09-07")
date_partial_lift = pd.to_datetime("2021-10-11")
date_full_lift = pd.to_datetime("2021-11-15")

date_cases = dfobs.date
new_cases = dfobs.new_cases
date_vacs = dfvacobs.date
vacs_prct = dfvacobs.prct

date_obs = dfobs.date
hosp_obs = dfobs.hosp
icu_obs = dfobs.icu
occ_icu_obs = dfobs.occ_icu


def plot_annotations(ax, dates, labels):
    for i, (date, label) in enumerate(zip(dates, labels)):

        if i % 2 == 0:
            ax.annotate(label,
                        xy=(date, 600), xycoords='data',
                        xytext=(date, 640), textcoords='data',
                        fontsize=7, va="center", ha="center", color="dimgray",
                        arrowprops=dict(facecolor='black', linewidth=0.5,
                                        arrowstyle="simple", connectionstyle="arc3"))
        else:
            ax.annotate(label,
                        xy=(date, 600), xycoords='data',
                        xytext=(date, 680), textcoords='data',
                        fontsize=7, va="center", ha="center", color="dimgray",
                        arrowprops=dict(facecolor='black', linewidth=0.5,
                                        arrowstyle="simple", connectionstyle="arc3"))


dates = [date_intro, date_lock, date_partial_lift, date_full_lift]
labels_dates = ["10 August \n Estimated virus \n introduction", "7 September \n Lockdown",
                "11 October \n Lockdown easing",
                "15 November \n End of lockdown"]

fig, ax1 = plt.subplots(figsize=(6, 4))
plt.rcParams.update({'font.size': 7})

ax2 = ax1.twinx()

ax1.scatter(date_cases, new_cases, color="#FF69B4", marker="x", s=10)

ax1.set_xlim(left=pd.to_datetime("2021-03-01"),
             right=pd.to_datetime("2022-01-01"))
ax1.set_ylim([-0.5, 600])
ax1.set_ylabel("Number of reported cases", fontsize=8, labelpad=4)

ax1.tick_params(axis="both", pad=4, labelsize=7)
ax1.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
ax1.yaxis.set_label_position("right")
ax1.yaxis.tick_right()
ax1.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))
ax1.grid(which='major', axis='both', color='#666666', linestyle="solid")
ax1.grid(which='minor', axis='both', color='#999999', alpha=0.2, linestyle="solid")
ax1.set_axisbelow(True)

ax2.plot(date_vacs, vacs_prct, color="#009966", lw=0.7)

ax2.set_ylabel("Vaccine coverage in the eligible population", fontsize=8, labelpad=3)
plot_annotations(ax1, dates, labels_dates)

ax2.tick_params(axis="both", direction="out", length=3)
ax2.yaxis.set_label_position("left")
ax2.yaxis.tick_left()
ax2.set_yticklabels(["0%", "10%", "20%", "30%", "40%",
                     "50%", "60%", "70%", "80%"], fontsize=8)
ax2.grid(visible=False)

# Create custom handles and labels
line1 = mlines.Line2D([], [], linestyle="solid", color='darkgreen', label='Vaccine rate', lw=1)
line2 = mlines.Line2D([], [], linestyle="None", marker="x", markersize=5,
                      color='#FF69B4', label='Daily cases', lw=1)

# Add legend with custom handles and labels
ax1.legend(handles=[line1, line2], fontsize="medium",
           frameon=True, handlelength=3, loc="upper left")

labels = ax1.get_xticklabels()
labels[0]._text = "01 Mar 2021"
labels[-1]._text = "01 Jan 2022"

ax1.set_xticklabels(labels)
plt.subplots_adjust(top=0.84, bottom=0.05, left=0.09, right=0.92)
plt.savefig(path + "/Figures/Fig1.tiff", format="tiff")
plt.close()

##########
# Figure 2
##########

icumax = 62
icu_norm = 20

plt.style.use("seaborn-whitegrid")
fig, [[ax00, ax01], [ax10, ax11]] = plt.subplots(nrows=2, ncols=2, figsize=(7, 6))
plt.rcParams.update({'font.size': 7})

ax00_twin = ax00.twinx()

# Cases
ax00.scatter(date_cases, new_cases, marker="x", s=5, color="#F08080", zorder=2)
ax00_twin.plot(dates_sim, minfs, color="#E9967A", zorder=3)


ax00.set_xlim(left=pd.to_datetime("2021-08-10"),
              right=pd.to_datetime("2021-12-30"))
ax00.set_ylim([-0.6, 600])
ax00.set_ylabel("Daily cases reported", fontsize=9, labelpad=1)
ax00_twin.set_ylabel("Daily cases simulated", fontsize=9, labelpad=9, rotation=270)
ax00.annotate("New cases reported and simulated", (0.01, 1.05),
              xycoords="axes fraction", fontsize=10)

ax00_twin.tick_params(axis="y", which="major", length=3)


# Hosp
ax01.scatter(date_obs, hosp_obs, s=5, marker="x", color="#9932CC", zorder=2)
ax01.plot(date_obs, mhosps, color="#EE82EE", lw=0.8, zorder=3)
ax01.fill_between(x=date_obs, y1=qhosps[0, :], y2=qhosps[1, :], alpha=0.3, color="#BA55D3")

ax01.set_xlim(left=pd.to_datetime("2021-09-01"),
              right=pd.to_datetime("2021-12-30"))
ax01.set_ylim([-0.1, 100])
ax01.set_ylabel("Daily admissions", fontsize=9, labelpad=9, rotation=270)
ax01.yaxis.set_label_position('right')
ax01.annotate("Daily hospital admissions", (0.01, 1.05),
              xycoords="axes fraction", fontsize=10)


# ICU
ax10.scatter(date_obs, icu_obs, s=5, marker="x", color="#6495ED", zorder=2)
ax10.plot(date_obs, micu, color="#00CED1", lw=0.8, zorder=3)
ax10.fill_between(x=date_obs, y1=qicu[0, :], y2=qicu[1, :], alpha=0.3, color="#5DADE2")
ax10.set_ylabel("Daily admissions", fontsize=9, labelpad=1)

ax10.set_xlim(left=pd.to_datetime("2021-09-01"),
              right=pd.to_datetime("2021-12-30"))
ax10.set_ylim([-0.05, 20])
ax10.annotate("Daily ICU admissions", (0.01, 1.05),
              xycoords="axes fraction", fontsize=10)

# occICU
ax11.axhline(icumax,
             linestyle='dashed', color="r", lw=0.8, zorder=1)
ax11.axhline(icu_norm, lw=0.8,
             linestyle='dashed', color="#CC5500", zorder=1)
ax11.scatter(date_obs, occ_icu_obs, s=5, marker="x", color="#6495ED", zorder=2)
ax11.plot(date_obs, mocc_icu, color="#483D8B", lw=0.8, zorder=3)
ax11.fill_between(x=date_obs, y1=qocc_icu[0, :], y2=qocc_icu[1, :], alpha=0.3, color="#7B68EE")

ax11.set_xlim(left=pd.to_datetime("2021-09-01"),
              right=pd.to_datetime("2021-12-30"))
ax11.set_ylim([-0.1, 110])

ax11.set_ylabel("Number of beds", fontsize=9, labelpad=1)
ax11.annotate("Daily ICU bed occupancy", (0.01, 1.05),
              xycoords="axes fraction", fontsize=10)

# Set the locale to the desired language, e.g., 'fr_FR' for French, 'es_ES' for Spanish, 'de_DE' for German
locale.setlocale(locale.LC_ALL, 'en_GB.utf8')

for ax in (ax00, ax01, ax10, ax11):

    if ax == ax00:
        ax.grid(False)
        ax00_twin.grid(False)
        ax.xaxis.set_major_locator(mdates.MonthLocator(interval=1))
        start_date = pd.to_datetime("2021-08-10")
        end_date = pd.to_datetime("2021-12-30")
        tick_locations = pd.date_range(start=start_date, end=end_date, freq='MS') + pd.Timedelta(days=14)
        # Set major ticks to the 15th of every month.
        ax.xaxis.set_minor_locator(plt.FixedLocator(mdates.date2num(tick_locations)))

    else:
        ax.grid(False)
        ax.xaxis.set_major_locator(mdates.MonthLocator(interval=1))
        start_date = pd.to_datetime("2021-09-01")
        end_date = pd.to_datetime("2021-12-30")
        tick_locations = pd.date_range(start=start_date, end=end_date, freq='MS') + pd.Timedelta(days=14)
        # Set major ticks to the 15th of every month.
        ax.xaxis.set_minor_locator(plt.FixedLocator(mdates.date2num(tick_locations)))

    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))

    ax.grid(axis="both", which="major", color="#999999")
    ax.grid(axis="both", which="minor", color="#666666", alpha=0.2)
    ax.set_axisbelow(True)
    # Format date
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
    ax.tick_params(axis="y", which="major", labelsize=7)

ax01.tick_params(axis="y", which="both", left=False, labelleft=False, right=True, labelright=True)

ax00.annotate("A", (0.01, 0.92), fontsize=12,
              xycoords="axes fraction", weight="bold")
ax01.annotate("B", (0.01, 0.92), fontsize=12,
              xycoords="axes fraction", weight="bold")
ax10.annotate("C", (0.01, 0.92), fontsize=12,
              xycoords="axes fraction", weight="bold")
ax11.annotate("D", (0.01, 0.92), fontsize=12,
              xycoords="axes fraction", weight="bold")

# Create custom handles and labels
line1 = mlines.Line2D([], [], linestyle="solid", color='darkgray', label='Simulation mean', lw=0.01)
rectangle1 = mpatches.Patch(color='darkgray', label='95% Cri')
line2 = mlines.Line2D([], [], linestyle="None", marker="x", markersize=3,
                      color='darkgray', label='Observations', lw=1)


# Add legend with custom handles and labels
ax01.legend(handles=[line1, rectangle1, line2],
            fontsize="medium", frameon=True, handlelength=1)

plt.subplots_adjust(top=0.96, bottom=0.03, left=0.07, right=0.94,
                    hspace=0.2, wspace=0.2)

plt.savefig(path + "/Figures/Fig2.tiff", format="tiff")
plt.close()


##########
# Figure 3
##########

locale.setlocale(locale.LC_ALL, 'en_GB.utf8')
icumax = 62
icu_norm = 20
plt.style.use("seaborn-whitegrid")
fig, (ax1, ax3) = plt.subplots(nrows=2, ncols=1, figsize=(7, 4), sharex=True)
plt.rcParams.update({'font.size': 7})

# Hospitalizations
ax1.plot(date_obs_sim, mhosps_nolock, color="m", zorder=1, lw=0.8)   # scenario 1
ax1.fill_between(x=date_obs_sim, y1=qhosps_nolock[0, :],
                 y2=qhosps_nolock[1, :], alpha=0.3, color="m", zorder=1)   # scenario 1

ax1.plot(date_obs_sim, mhosps_vac_partial, color="darkorange", zorder=2, lw=0.8)  # scenario 2
ax1.fill_between(x=date_obs_sim, y1=qhosps_vac_partial[0, :],
                 y2=qhosps_vac_partial[1, :], alpha=0.3, color="darkorange", zorder=2)  # scenario 2

ax1.plot(date_obs_sim, mhosps_sooner, color="c", zorder=3, lw=0.5)  # scenario 3
ax1.fill_between(x=date_obs_sim, y1=qhosps_sooner[0, :],
                 y2=qhosps_sooner[1, :], alpha=0.3, color="c", zorder=3)  # scenario 3

ax1.plot(date_obs_sim, mhosps, ls=(0, (1, 1)), color="darkgreen", lw=2.5)  # baseline

ax1.set_ylabel("Daily admissions", labelpad=1, fontsize=9)


# ICU occupancy

ax3.axhline(icumax, linestyle='dashed', color="r", lw=0.8, zorder=1)
ax3.axhline(icu_norm, lw=0.8, linestyle='dashed', color="#CC5500", zorder=1)

ax3.plot(date_obs_sim, mocc_icu_nolock_alt, color="m", zorder=1, lw=0.8)   # scenario1
ax3.fill_between(x=date_obs_sim, y1=qocc_icu_nolock_alt[0, :],
                 y2=qocc_icu_nolock_alt[1, :], alpha=0.3, color="m", zorder=1)   # scenario1

ax3.plot(date_obs_sim, mocc_icu_vac_partial_alt, color="darkorange", zorder=2, lw=0.8)  # scenario 2
ax3.fill_between(x=date_obs_sim, y1=qocc_icu_vac_partial_alt[0, :],
                 y2=qocc_icu_vac_partial_alt[1, :], alpha=0.3, color="darkorange", zorder=2)  # scenario 2

ax3.plot(date_obs_sim, mocc_icu_sooner_alt, color="c", zorder=3, lw=0.8)  # scenario 3
ax3.fill_between(x=date_obs_sim, y1=qocc_icu_sooner_alt[0, :],
                 y2=qocc_icu_sooner_alt[1, :], alpha=0.3, color="c", zorder=3)  # scenario 3
ax3.set_ylabel("Number of beds", labelpad=1, fontsize=9)

ax3.plot(date_obs_sim, mocc_icu_alt, ls=(0, (1, 1)), color="darkgreen", lw=2.5)   # baseline


for ax in (ax1, ax3):
    ax.grid(False)
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=1))
    start_date = "2021-09-01"
    end_date = date_obs_sim.iloc[-1]
    tick_locations = pd.date_range(start=start_date, end=end_date, freq='MS') + pd.Timedelta(days=14)
    # Set major ticks to the 15th of every month.
    ax.xaxis.set_minor_locator(plt.FixedLocator(mdates.date2num(tick_locations)))

    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))

    ax.grid(axis="both", which="major", color="#999999")
    ax.grid(axis="both", which="minor", color="#666666", alpha=0.2)
    ax.set_axisbelow(True)
    # Format date
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
    ax.tick_params(axis="x", which="both", labelsize=9)

ax1.annotate("Daily hospital admissions", (0.01, 1.02),
             xycoords="axes fraction", fontsize=9)
# ax2.annotate("Daily ICU admissions", (0.01, 1.02),
#              xycoords="axes fraction", fontsize=9)
ax3.annotate("Daily ICU occupancy", (0.01, 1.02),
             xycoords="axes fraction", fontsize=9)

ax1.annotate("A", (0.01, 0.91), fontsize=12,
             xycoords="axes fraction", weight="bold")
ax3.annotate("B", (0.01, 0.91), fontsize=12,
             xycoords="axes fraction", weight="bold")

# Create custom handles and labels
line1 = mlines.Line2D([], [], linestyle=(0, (1, 1)), color='darkgreen', label='Simulation baseline', lw=1)
line2 = mlines.Line2D([], [], linestyle="solid",  color='darkgray', label='Simulations mean', lw=1)
rectangle1 = mpatches.Patch(color='m', label='No lockdown')
rectangle2 = mpatches.Patch(color='darkorange', label='Vaccination stop')
rectangle3 = mpatches.Patch(color='c', label='Early intervention')

# Add legend with custom handles and labels
ax1.legend(handles=[line1, line2, rectangle1, rectangle2, rectangle3],
           fontsize="medium", frameon=True, handlelength=3)

line3 = mlines.Line2D([], [], linestyle="dashed", color='#CC5500', label='Normal ICU capacity', lw=1)
line4 = mlines.Line2D([], [], linestyle="dashed",  color='r', label='Maximum ICU capacity', lw=1)

ax3.legend(handles=[line3, line4], fontsize="medium", frameon=True, handlelength=3)

plt.subplots_adjust(top=0.92, bottom=0.05, left=0.07, right=0.98,
                    hspace=0.2, wspace=0.2)
plt.savefig(path + "/Figures/Fig3.tiff", format="tiff")
plt.close()


##########
# Figure Supplementary 2
##########

icumax = 62
icu_norm = 20

plt.style.use("seaborn-whitegrid")
fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(7, 4))
plt.rcParams.update({'font.size': 9})

# ICU
ax1.scatter(date_obs, icu_obs, s=5, marker="x", color="#6495ED", zorder=2)
ax1.plot(date_obs, micu, color="#48D1CC", lw=0.8, zorder=3)
ax1.fill_between(x=date_obs, y1=qicu[0, :], y2=qicu[1, :], alpha=0.3, color="#48D1CC")

ax1.plot(date_obs, micu_alt, color="#CD5C5C", lw=0.8, zorder=3)
ax1.fill_between(x=date_obs, y1=qicu_alt[0, :], y2=qicu_alt[1, :], alpha=0.3, color="#CD5C5C")

ax1.set_ylabel("Daily admissions", fontsize=9, labelpad=1)
ax1.set_xlim(left=pd.to_datetime("2021-09-01"),
             right=pd.to_datetime("2021-12-30"))
ax1.set_ylim([-0.05, 20])
ax1.annotate("Daily ICU admissions", (0.01, 1.05),
             xycoords="axes fraction", fontsize=10)

# occICU
ax2.axhline(icumax,
            linestyle='dashed', color="r", lw=0.8, zorder=1)
ax2.axhline(icu_norm, lw=0.8,
            linestyle='dashed', color="#CC5500", zorder=1)
ax2.scatter(date_obs, occ_icu_obs, s=5, marker="x", color="#6495ED", zorder=2)
ax2.plot(date_obs, mocc_icu, color="#48D1CC", lw=0.8, zorder=3)
ax2.fill_between(x=date_obs, y1=qocc_icu[0, :], y2=qocc_icu[1, :], alpha=0.3, color="#48D1CC")

ax2.plot(date_obs, mocc_icu_alt, color="#CD5C5C", lw=0.8, zorder=3)
ax2.fill_between(x=date_obs, y1=qocc_icu_alt[0, :], y2=qocc_icu_alt[1, :], alpha=0.3, color="#CD5C5C")

ax2.set_xlim(left=pd.to_datetime("2021-09-01"),
             right=pd.to_datetime("2021-12-30"))
ax2.set_ylim([-0.1, 110])

ax2.set_ylabel("Number of beds", fontsize=9, labelpad=1)
ax2.annotate("Daily ICU bed occupancy", (0.01, 1.05),
             xycoords="axes fraction", fontsize=10)

# Set the locale to the desired language, e.g., 'fr_FR' for French, 'es_ES' for Spanish, 'de_DE' for German
locale.setlocale(locale.LC_ALL, 'en_GB.utf8')

for ax in (ax1, ax2):

    ax.grid(False)
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=1))
    start_date = pd.to_datetime("2021-09-01")
    end_date = pd.to_datetime("2021-12-30")
    tick_locations = pd.date_range(start=start_date, end=end_date, freq='MS') + pd.Timedelta(days=14)
    # Set major ticks to the 15th of every month.
    ax.xaxis.set_minor_locator(plt.FixedLocator(mdates.date2num(tick_locations)))

    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))

    ax.grid(axis="both", which="major", color="#999999")
    ax.grid(axis="both", which="minor", color="#666666", alpha=0.2)
    ax.set_axisbelow(True)
    # Format date
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
    ax.tick_params(axis="y", which="major", labelsize=9)

ax1.annotate("A", (0.01, 0.91), fontsize=12,
             xycoords="axes fraction", weight="bold")
ax2.annotate("B", (0.01, 0.91), fontsize=12,
             xycoords="axes fraction", weight="bold")

# Create custom handles and labels
line1 = mlines.Line2D([], [], linestyle="solid", color='#48D1CC', label='daily $p$ICU', lw=1)
line2 = mlines.Line2D([], [], linestyle="solid", color='#CD5C5C', label='average $p$ICU', lw=1)
rectangle1 = mpatches.Patch(color='darkgray', label='95% Cri')
line3 = mlines.Line2D([], [], marker="x",  linestyle="None", color='darkgray',
                      label='Observations', markersize=5)
line4 = mlines.Line2D([], [], linestyle="dashed", color='#CC5500', label='Normal ICU capacity', lw=1)
line5 = mlines.Line2D([], [], linestyle="dashed", color='r', label='Maximum ICU capacity', lw=1)

# Add legend with custom handles and labels
ax1.legend(handles=[line1,  line2, rectangle1, line3, line4, line5],
           fontsize="x-small", frameon=True, handlelength=3)


plt.subplots_adjust(top=0.94, bottom=0.05, left=0.07, right=0.94,
                    hspace=0.25, wspace=0.2)

plt.savefig(path + "/Figures/FigSup2.tiff", format="tiff")
plt.close()


##########
# Figure Supplementary 3
##########

from simICU2023 import *

mu_tauHosp_ICU = 0 + 0.5
pICU = estimate_pICU(hosp_obs, icu_obs, tauHosp_ICU=mu_tauHosp_ICU, window=1)
pICU[pICU < 0] = 0
mean_pICU = np.sum(icu_obs) / np.sum(hosp_obs)
cap_icu = dfobs.cap_icu

icumax = 62
icu_norm = 20

plt.style.use("seaborn-whitegrid")

_, ax = plt.subplots(figsize=(7, 4))
ax2 = ax.twinx()

ax.bar(date_obs, cap_icu, color="#FFE4E1", edgecolor="None", alpha=0.9, zorder=1)
ax.scatter(date_obs, occ_icu_obs, marker="x", color="gray", label="ICU bed occupancy", s=20)
ax.set_ylim([0, 70])

line, = ax2.plot(date_obs, pICU, color="#48D1CC", marker="o", alpha=0.8,
                 label="Daily probability of ICU bed \n occupancy given hospitalization", zorder=3)
ax2.axhline(mean_pICU, ls="solid", lw=2.5, alpha=0.7, color="#4682B4")

ax2.set_xlim(left=pd.to_datetime("2021-09-01"),
             right=pd.to_datetime("2021-12-30"))

ax.set_ylabel("Probability", size=10)
ax2.set_ylabel("Number of beds", size=10)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))

ax.grid(False)
ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))

ax.grid(axis="both", which="major", color="#999999")
ax.grid(axis="both", which="minor", color="#666666", alpha=0.2)
ax2.tick_params(axis="y", which="major", length=3)
ax.tick_params(axis="x", which="major", length=3)

# Create custom handles and labels
line1 = mlines.Line2D([], [], linestyle="solid", marker="o", color='#48D1CC', label='daily $P_{ICU}$',
                      markersize=4, lw=0.8)
line2 = mlines.Line2D([], [], linestyle="solid", color='#4682B4', label='average $P_{ICU}$', lw=0.8)
line3 = mlines.Line2D([], [], marker="x",  linestyle="None", color='darkgray',
                      label='Observations', markersize=5)
rectangle1 = mpatches.Patch(color='#FFE4E1', label='ICU bed capacity')


# Add legend with custom handles and labels
ax.legend(handles=[line1,  line2, line3, rectangle1],
           fontsize="xx-small", frameon=True, handlelength=1, loc="upper left")

ax.set_axisbelow(True)

plt.subplots_adjust(top=0.92, bottom=0.05, left=0.07, right=0.95,
                    hspace=0.2)
plt.savefig(path + "/Figures/FigSup3.tiff", format="tiff")
plt.close()


##########
# Figure Supplementary 4
##########

from simICU2023 import *
mu_tauHosp_ICU = 0.5
pICU = estimate_pICU(hosp_obs, icu_obs, tauHosp_ICU=mu_tauHosp_ICU, window=1)
pICU[pICU < 0] = 0
mean_pICU = np.sum(icu_obs) / np.sum(hosp_obs)

icumax = 62
icu_norm = 20

_, ax = plt.subplots(figsize=(6, 4))

ax2 = ax.twinx()

ax.set_xlim(left=pd.to_datetime("2021-09-01"),
             right=pd.to_datetime("2021-12-30"))
ax.set_ylim([0, 70])
ax.set_ylabel("Daily hospital admissions", size=10)

line, = ax2.plot(date_obs, pICU, color="#48D1CC", marker="o", alpha=0.8)
ax2.axhline(mean_pICU, ls="solid", lw=2.5, alpha=0.7, color="#4682B4")
ax2.grid(False)

ax2.tick_params(axis="y", which="major", length=3)
ax2.set_ylabel("Probability", size=10)

ax.scatter(date_obs, hosp_obs, marker="x", color="gray", s=20)

ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))

ax.grid(False)
ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))
ax.tick_params(axis="x", which="major", length=3)

ax.grid(axis="both", which="major", color="#999999")
ax.grid(axis="both", which="minor", color="#666666", alpha=0.2)
ax.set_axisbelow(True)

# Create custom handles and labels
line1 = mlines.Line2D([], [], linestyle="solid", marker="o", color='#48D1CC', label='daily $P_{ICU}$', lw=0.2)
line2 = mlines.Line2D([], [], linestyle="solid", color='#4682B4', label='average $P_{ICU}$', lw=1.5)
line3 = mlines.Line2D([], [], marker="x",  linestyle="None", color='darkgray',
                      label='Observations', markersize=5)


# Add legend with custom handles and labels
ax.legend(handles=[line1,  line2, line3],
           fontsize="medium", frameon=True, handlelength=1, loc=(0.3, 0.83))

plt.subplots_adjust(top=0.92, bottom=0.05, left=0.07, right=0.92,
                    hspace=0.2)
plt.savefig(path + "/Figures/FigSup4.tiff", format="tiff")
plt.close()


##########
# Figure Supplementary 5
##########

locale.setlocale(locale.LC_ALL, 'en_GB.utf8')
icumax = 62
icu_norm = 20
plt.style.use("seaborn-whitegrid")
fig, (ax1, ax3) = plt.subplots(nrows=2, ncols=1, figsize=(7, 4), sharex=True)
plt.rcParams.update({'font.size': 7})

# Hospitalizations
ax1.plot(date_obs_sim, mhosps_nolock, color="m", zorder=1, lw=0.8)   # scenario 1
ax1.fill_between(x=date_obs_sim, y1=qhosps_nolock[0, :],
                 y2=qhosps_nolock[1, :], alpha=0.3, color="m", zorder=1)   # scenario 1

ax1.plot(date_obs_sim, mhosps_vac_partial, color="darkorange", zorder=2, lw=0.8)  # scenario 2
ax1.fill_between(x=date_obs_sim, y1=qhosps_vac_partial[0, :],
                 y2=qhosps_vac_partial[1, :], alpha=0.3, color="darkorange", zorder=2)  # scenario 2

ax1.plot(date_obs_sim, mhosps_sooner, color="c", zorder=3, lw=0.5)  # scenario 3
ax1.fill_between(x=date_obs_sim, y1=qhosps_sooner[0, :],
                 y2=qhosps_sooner[1, :], alpha=0.3, color="c", zorder=3)  # scenario 3
ax1.set_ylabel("Daily admissions", labelpad=1, fontsize=9)
ax1.plot(date_obs_sim, mhosps, ls=(0, (1, 1)), color="darkgreen", lw=2.5)  # baseline

# ICU occupancy
ax3.axhline(icumax, linestyle='dashed', color="r", lw=0.8, zorder=1)
ax3.axhline(icu_norm, lw=0.8, linestyle='dashed', color="#CC5500", zorder=1)

ax3.plot(date_obs_sim, mocc_icu_nolock, color="m", zorder=1, lw=0.8)   # scenario1
ax3.fill_between(x=date_obs_sim, y1=qocc_icu_nolock[0, :],
                 y2=qocc_icu_nolock[1, :], alpha=0.3, color="m", zorder=1)   # scenario1

ax3.plot(date_obs_sim, mocc_icu_vac_partial, color="darkorange", zorder=2, lw=0.8)  # scenario 2
ax3.fill_between(x=date_obs_sim, y1=qocc_icu_vac_partial[0, :],
                 y2=qocc_icu_vac_partial[1, :], alpha=0.3, color="darkorange", zorder=2)  # scenario 2

ax3.plot(date_obs_sim, mocc_icu_sooner, color="c", zorder=3, lw=0.8)  # scenario 3
ax3.fill_between(x=date_obs_sim, y1=qocc_icu_sooner[0, :],
                 y2=qocc_icu_sooner[1, :], alpha=0.3, color="c", zorder=3)  # scenario 3
ax3.plot(date_obs_sim, mocc_icu, ls=(0, (1, 1)), color="darkgreen", lw=2.5)   # baseline


ax3.set_ylabel("Number of beds", labelpad=1, fontsize=9)

for ax in (ax1, ax3):
    ax.grid(False)
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=1))
    start_date = "2021-09-01"
    end_date = date_obs_sim.iloc[-1]
    tick_locations = pd.date_range(start=start_date, end=end_date, freq='MS') + pd.Timedelta(days=14)
    # Set major ticks to the 15th of every month.
    ax.xaxis.set_minor_locator(plt.FixedLocator(mdates.date2num(tick_locations)))

    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))

    ax.grid(axis="both", which="major", color="#999999")
    ax.grid(axis="both", which="minor", color="#666666", alpha=0.2)
    ax.set_axisbelow(True)
    # Format date
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
    ax.tick_params(axis="x", which="both", labelsize=9)

ax1.annotate("Daily hospital admissions", (0.01, 1.02),
             xycoords="axes fraction", fontsize=9)

ax3.annotate("Daily ICU occupancy", (0.01, 1.02),
             xycoords="axes fraction", fontsize=9)


# Create custom handles and labels
line1 = mlines.Line2D([], [], linestyle=(0, (1, 1)), color='darkgreen', label='Simulation baseline', lw=1)
line2 = mlines.Line2D([], [], linestyle="solid",  color='darkgray', label='Simulations mean', lw=1)
rectangle1 = mpatches.Patch(color='m', label='No lockdown')
rectangle2 = mpatches.Patch(color='darkorange', label='Vaccination stop')
rectangle3 = mpatches.Patch(color='c', label='Early intervention')

# Add legend with custom handles and labels
ax1.legend(handles=[line1, line2, rectangle1, rectangle2, rectangle3],
           fontsize="medium", frameon=True, handlelength=3)

line3 = mlines.Line2D([], [], linestyle="dashed", color='#CC5500', label='Normal ICU capacity', lw=1)
line4 = mlines.Line2D([], [], linestyle="dashed",  color='r', label='Maximum ICU capacity', lw=1)

ax3.legend(handles=[line3, line4], fontsize="medium", frameon=True, handlelength=3)

plt.subplots_adjust(top=0.92, bottom=0.05, left=0.07, right=0.98,
                    hspace=0.2, wspace=0.2)
plt.savefig(path + "/Figures/FigSup5.tiff", format="tiff")
plt.close()
