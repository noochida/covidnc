import numpy as np

n_a = 6  # age groups
n_s = 38  # statuts


def SEIR(t, y, pHosps, susceps, protecs, ls, infFunc, vacFunc, R0Func):

    """
    SEIHR

    input: t, y, pHosps, susceps, protecs, vacFuncs, infFuncs, (R0, a1)

    output: S, E1, E2, Imild, Ihosp, H, R, Icumul, Hcumul (.,V0,V1,V2), SV1cumul, SV2cumul

    """

    # Define total simulation time and sample times.
    lsimul = np.sum(ls)
    sample_times = np.arange(lsimul)

    # Population of New Caledonia
    N = 271407

    # Incubation, Presymptomatic, Symptomatic, Hospital stay period
    tr1, tr2, tr3, tr_Hout = 4, 1, 3, 8
    r1, r2, r3, r_Hout = 1 / tr1, 1 / tr2, 1 / tr3, 1 / tr_Hout

    # Total infectious period
    tInf = tr3 + tr2

    # Transmission rate
    Rj = R0Func(t) / tInf

    # Set vaccine protection parameters
    infVac, suscepVac1, sevVac1, suscepVac2, sevVac2 = protecs

    tVac1, tVac2 = 1/14, 1/14

    dydt = np.zeros(len(y))
    age_indices = n_s * np.arange(n_a)

    # Infectious totals across age groups according to vaccine statuts
    E2tot, E2V0tot, E2V1tot, E2V2tot = np.sum([y[i + age_indices] for i in [2, 11, 20, 29]], axis=1)
    Imildtot, ImildV0tot, ImildV1tot, ImildV2tot = np.sum([y[i + age_indices] for i in [3, 12, 21, 30]], axis=1)
    Ihosptot, IhospV0tot, IhospV1tot, IhospV2tot = np.sum([y[i + age_indices] for i in [4, 13, 22, 31]], axis=1)

    # print(E2tot)
    # Total number of infectious individuals
    Inum = ((E2tot + Imildtot + Ihosptot) +
            (E2V0tot + ImildV0tot + IhospV0tot) +
            (1-infVac) * (E2V1tot + ImildV1tot + IhospV1tot) +
            (1-infVac) * (E2V2tot + ImildV2tot + IhospV2tot))

    # FOI
    Itot = Inum / N

    # Just retrieve non cumulative variables (7/9)
    var_idx = np.array([i + np.arange(7) for i in np.arange(0, 28, 9)]).ravel()

    S, E1, E2, Imild, Ihosp, H, R, \
    SV0, E1V0, E2V0, ImildV0, IhospV0, HV0, RV0,\
    SV1, E1V1, E2V1, ImildV1, IhospV1, HV1, RV1, \
    SV2, E1V2, E2V2, ImildV2, IhospV2, HV2, RV2 = y[np.array([i + age_indices for i in var_idx])]

    # Icumul = y[7 + age_indices]
    # Hcumul = y[8 + age_indices]
    # IcumulV0 = y[16 + age_indices]
    # HcumulV0 = y[17 + age_indices]
    # IcumulV1 = y[25 + age_indices]
    # HcumulV1 = y[26 + age_indices]
    # IcumulV2 = y[34 + age_indices]
    # HcumulV2 = y[35 + age_indices]
    # SV0cumul = y[36 + age_indices]
    # SV2cumul = y[37 + age_indices]

    tot_non_vac = S + E1 + E2 + Imild + Ihosp + H + R

    dydt[0 + age_indices] = -(Rj * susceps * S * Itot) - infFunc(t) - (vacFunc(t) * S / tot_non_vac)
    dydt[1 + age_indices] = (Rj * susceps * S * Itot) - (r1 * E1) - (vacFunc(t) * E1 / tot_non_vac)
    dydt[2 + age_indices] = infFunc(t) + (r1 * E1) - (r2 * E2) - (vacFunc(t) * E2 / tot_non_vac)
    dydt[3 + age_indices] = ((1 - pHosps) * r2 * E2) - (r3 * Imild) - (vacFunc(t) * Imild / tot_non_vac)
    dydt[4 + age_indices] = (pHosps * r2 * E2) - (r3 * Ihosp) - (vacFunc(t) * Ihosp / tot_non_vac)
    dydt[5 + age_indices] = (r3 * Ihosp) - (r_Hout * H) - (vacFunc(t) * H / tot_non_vac)
    dydt[6 + age_indices] = (r3 * Imild) + (r_Hout * H) - (vacFunc(t) * R / tot_non_vac)
    dydt[7 + age_indices] = (r2 * E2)
    dydt[8 + age_indices] = (r3 * Ihosp)

    dydt[9 + age_indices] = (vacFunc(t) * S / tot_non_vac) - (Rj * susceps * SV0 * Itot) - (tVac1 * SV0)
    dydt[10 + age_indices] = ((vacFunc(t) * E1 / tot_non_vac) + (Rj * susceps * SV0 * Itot)
                                         - (r1 * E1V0) - (tVac1 * E1V0))
    dydt[11 + age_indices] = (vacFunc(t) * E2 / tot_non_vac) + (r1 * E1V0) - (r2 * E2V0) - (tVac1 * E2V0)
    dydt[12 + age_indices] = ((vacFunc(t) * Imild / tot_non_vac) + ((1 - pHosps) * r2 * E2V0)
                                         - (r3 * ImildV0) - (tVac1 * ImildV0))
    dydt[13 + age_indices] = ((vacFunc(t) * Ihosp / tot_non_vac) + (pHosps * r2 * E2V0)
                                         - (r3 * IhospV0) - (tVac1 * IhospV0))
    dydt[14 + age_indices] = (vacFunc(t) * H / tot_non_vac) + (r3 * IhospV0) - (r_Hout * HV0) - (tVac1 * HV0)
    dydt[15 + age_indices] = (vacFunc(t) * R / tot_non_vac) + (r3 * ImildV0) + (r_Hout * HV0) - (tVac1 * RV0)
    dydt[16 + age_indices] = (r2 * E2V0)
    dydt[17 + age_indices] = (r3 * IhospV0)

    dydt[18 + age_indices] = (tVac1 * SV0) - (Rj * susceps * (1 - suscepVac1) * SV1 * Itot) - (tVac2 * SV1)
    dydt[19 + age_indices] = ((tVac1 * E1V0) + (Rj * susceps * (1 - suscepVac1) * SV1 * Itot)
                                         - (r1 * E1V1) - (tVac2 * E1V1))
    dydt[20 + age_indices] = (tVac1 * E2V0) + (r1 * E1V1) - (r2 * E2V1) - (tVac2 * E2V1)
    dydt[21 + age_indices] = ((tVac1 * ImildV0) + ((1 - (pHosps * (1 - sevVac1))) * r2 * E2V1)
                                         - (r3 * ImildV1) - (tVac2 * ImildV1))
    dydt[22 + age_indices] = ((tVac1 * IhospV0) + ((pHosps * (1 - sevVac1)) * r2 * E2V1)
                                         - (r3 * IhospV1) - (tVac2 * IhospV1))
    dydt[23 + age_indices] = (tVac1 * HV0) + (r3 * IhospV1) - (r_Hout * HV1) - (tVac2 * HV1)
    dydt[24 + age_indices] = (tVac1 * RV0) + (r3 * ImildV1) + (r_Hout * HV1) - (tVac2 * RV1)
    dydt[25 + age_indices] = (r2 * E2V1)
    dydt[26 + age_indices] = (r3 * IhospV1)

    dydt[27 + age_indices] = (tVac2 * SV1) - (Rj * susceps * (1 - suscepVac2) * SV2 * Itot)
    dydt[28 + age_indices] = (tVac2 * E1V1) + (Rj * susceps * (1 - suscepVac2) * SV2 * Itot) - (r1 * E1V2)
    dydt[29 + age_indices] = (tVac2 * E2V1) + (r1 * E1V2) - (r2 * E2V2)
    dydt[30 + age_indices] = ((tVac2 * ImildV1) + ((1 - (pHosps * (1 - sevVac2))) * r2 * E2V2)
                                         - (r3 * ImildV2))
    dydt[31 + age_indices] = (tVac2 * IhospV1) + ((pHosps * (1 - sevVac2)) * r2 * E2V2) - (r3 * IhospV2)
    dydt[32 + age_indices] = (tVac2 * HV1) + (r3 * IhospV2) - (r_Hout * HV2)
    dydt[33 + age_indices] = (tVac2 * RV1) + (r3 * ImildV2) + (r_Hout * HV2)
    dydt[34 + age_indices] = (r2 * E2V2)
    dydt[35 + age_indices] = (r3 * IhospV2)

    dydt[36 + age_indices] = ((vacFunc(t) * S / tot_non_vac)
                                         + (vacFunc(t) * E1 / tot_non_vac)
                                         + (vacFunc(t) * E2 / tot_non_vac)
                                         + (vacFunc(t) * Imild / tot_non_vac)
                                         + (vacFunc(t) * Ihosp / tot_non_vac)
                                         + (vacFunc(t) * H / tot_non_vac)
                                         + (vacFunc(t) * R / tot_non_vac))

    dydt[37 + age_indices] = ((tVac2 * SV1)
                                         + (tVac2 * E1V1)
                                         + (tVac2 * E2V1)
                                         + (tVac2 * ImildV1)
                                         + (tVac2 * IhospV1)
                                         + (tVac2 * HV1)
                                         + (tVac2 * RV1))

    # print(np.sum(dydt, axis=0))
    return dydt


def switch_eta(t, t1, eta, nu, k=1):
    switch = eta / (1 + np.exp(-k * (t - t1 - nu)))
    return switch