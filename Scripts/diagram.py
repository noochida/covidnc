from graphviz import Digraph

dot = Digraph()
dot.attr('graph', size='7,7')
"""
ranksep='0.75', nodesep='0.50', 
"""

compartments = ['S', 'E1', 'E2', 'Imild', 'Ihosp', 'H']
legend = 'Age groups: \n 0-11, 12-15, 16-49, \n 50-64, 65-74, 75+ y.o \n \n' \
             'Vaccination status: \n unvaccinated, V0, V1, V2'
# vacc_status = 'Vaccination status: Unvaccinated, V0, V1, V2'
colors = ['lightblue', 'lightblue', 'lightcoral', 'lightcoral',
          'lightcoral', 'lightblue']


# Create nodes for compartments
for compartment, color in zip(compartments, colors):
    dot.node(compartment, compartment, shape='rectangle', color=color,
             fillcolor=color, style='filled')

dot.node('ICU', 'ICU', shape='rectangle', color='lightblue')
dot.node('R', 'R', shape='rectangle', color="lightblue", style="filled")

# dot.node('R', 'R', shape='point', width='0', height='0')  # Invisible node
dot.node('ICU_R', 'ICU_R', shape='point', width='0', height='0')  # Invisible node
# Create edges

dot.edge('S', 'E1', label='&lambda; &times; S',  fontsize='10')
dot.edge('E1', 'E2', label='<&sigma;<SUB>1</SUB> &times; E1>',  fontsize='10')
dot.edge('E2', 'Imild', label='<&sigma;<sub>2</sub> &times; E2 &times; (1-P<sub>hosp</sub>)>',  fontsize='10')
dot.edge('E2', 'Ihosp', label='<&sigma;<sub>2</sub> &times; E2 &times; P<sub>hosp</sub>>',  fontsize='10')
dot.edge('Imild', 'R', label='<&gamma; &times; Imild>',  fontsize='10')
dot.edge('Ihosp', 'H', label='<&gamma; &times; Ihosp>',  fontsize='10')
dot.edge('H', 'R', label='<&tau;<sub>hosp-R</sub> &times; H>',  fontsize='10')
dot.edge('H', 'ICU', label='<P<sub>ICU</sub>>',  fontsize='10',
         _attributes={'style': 'dashed', 'color': 'dodgerblue'})
dot.edge('ICU', 'ICU_R', label='<&tau;<sub>ICU-R</sub>>',  fontsize='10',
         _attributes={'style': 'dashed', 'color': 'dodgerblue'})

# Add legend for age groups and vaccination status
dot.node('legend', legend, shape='plaintext')
# dot.node('legend_vacc', vacc_status, shape='plaintext')

# # Add edges for the legend
# dot.edges([('legend_age', 'legend_vacc')])

# Adjust layout for horizontal diagram
# dot.attr(rankdir='LR')

# path to directory
path = ".../COVIDNC"

# Render and save the diagram
dot.render(path + "/FigSup6", view=True, format='png')


