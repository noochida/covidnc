from scipy.stats import expon
from scipy.stats import poisson
from scipy.stats import norm
import numpy as np


def estimate_pICU(iHosp, iICU, tauHosp_ICU, window):
    if (len(iHosp) != len(iICU)):
        raise ValueError("ERROR: check length of iHosp and iICU!")

    n_steps = len(iHosp)
    pICU = np.zeros(len(iHosp))

    # La
    ws = np.zeros(n_steps)
    for step in range(n_steps):
        if step == 0:
            x1 = 0
        else:
            x1 = step - 0.5
        x2 = step + 0.5
        # Probabilité d'aller en réanimation de t0 jusqu'à t_nsteps
        ws[step] = (expon.cdf(x2, scale=tauHosp_ICU)
                    - expon.cdf(x1, scale=tauHosp_ICU))

    # Pour chaque arrivé à t_step (iHosp[step]), on calcule la proportion de
    # cette arrivée qui va en réa de t_step jusqu'à t_final
    the_sum = np.zeros(n_steps)
    for step in range(n_steps):
        for curr in range(step, n_steps):   # ws de step à t_final
            the_sum[curr] += iHosp[step] * ws[curr - step]

    for step in range(n_steps):
        num = 0.00
        den = 0.00
        for curr in range(window):  # fenêtre glissante 3j en arrière
            if (step - curr) < 0:
                break
            num += iICU[step - curr]
            den += the_sum[step - curr]

        if ((den > 0) & (den >= num)):
            pICU[step] = num / den
        else:
            pICU[step] = -1

    return pICU


def simulate_iICU(iHosp, pICU, tauHosp_ICU, draw=False):

    if (len(iHosp) != len(pICU)):
        raise ValueError("ERROR: check length of iHosp and pICU")

    n_steps = len(iHosp)

    # Proba hosp -> ICU
    ws = np.zeros(n_steps)
    for step in range(n_steps):
        if step == 0:
            x1 = 0
        else:
            x1 = step - 0.5
        x2 = step + 0.5
        ws[step] = (expon.cdf(x2, scale=tauHosp_ICU)
                    - expon.cdf(x1, scale=tauHosp_ICU))

    iICU = np.zeros(n_steps)

    for step in range(n_steps):
        for curr in range(step, n_steps):
            iICU[curr] += pICU[step] * iHosp[step] * ws[curr - step]
        if draw:
            iICU[step] = poisson.rvs(iICU[step])

    return iICU


def simulate_inICU(iICU, spoints_tauICU_R, mu_tauICU_R,
                   window=1, draw=True, debug=False):

    n_changes = len(spoints_tauICU_R)

    if len(mu_tauICU_R) != (n_changes + 1):
        raise ValueError("ERROR: mu_tauICU_R is not sized correctly!")

    if n_changes != 0:
        # Vérifie que l'écart entre le point de changement actuel
        # et le précédent est plus grand que la fenêtre glissante
        for curr_cp in range(n_changes-1):
            diff = spoints_tauICU_R[curr_cp + 1] - spoints_tauICU_R[curr_cp]
            if (diff < window):
                raise ValueError(
                    "ERROR: window is too big for current change points!")

        n_steps = len(iICU)
        # Matrice de poids pour les différents durée de séjour
        pSurv = np.zeros((n_changes+1, n_steps))
        # avec changement linéaire de la taille de la fenêtre
        for curr_cp in range(n_changes):
            curr_s = spoints_tauICU_R[curr_cp]
            if curr_cp == 0:
                prev_s_w = 0
            else:
                prev_s_w = spoints_tauICU_R[curr_cp - 1] + window
            for step in range(prev_s_w, curr_s):
                pSurv[curr_cp, step] = 1
            # Ne pas dépasser n_steps (cas: n_steps + window)
            curr_s_w = min(curr_s + window, n_steps)
            for step in range(curr_s, curr_s_w):
                # Changement linéaire de la taille de la fenêtre
                lin_change = (step - curr_s) / window
                pSurv[curr_cp, step] = 1 - lin_change
                pSurv[curr_cp + 1, step] = lin_change

        prev_s_w = spoints_tauICU_R[n_changes - 1] + window
        for step in range(prev_s_w, n_steps):
            pSurv[n_changes, step] = 1
        # Somme les colonnes et vérifié qu'elle est égale à 1.
        if (debug):
            for step in range(n_steps):
                the_sum = 0
                for curr_cp in range(n_changes + 1):
                    the_sum += pSurv[curr_cp, step]
                if (np.abs(the_sum - 1) > 1e-3):
                    raise ValueError(
                        "ERROR: simulate_inICU ->"
                        " problem with pSurv normalization!")
    else:
        n_steps = len(iICU)
        pSurv = np.ones((n_changes+1, n_steps))

    inICU_ave = np.zeros(n_steps)
    # Survival function for ICU -> recovery
    surv_R = np.zeros((n_changes+1, n_steps))
    for cp in range(n_changes+1):
        for step in range(n_steps):
            if step == 0:
                x = 0.5
            else:
                x = step
            # Probabilité de recover après t_step
            surv_R[cp, step] = 1 - expon.cdf(x, scale=mu_tauICU_R[cp])

    for step in range(n_steps):
        for curr in range(step, n_steps):
            for cp in range(n_changes+1):
                inICU_ave[curr] += (iICU[step] * pSurv[cp, curr]
                                    * surv_R[cp, curr - step])

        if draw:
            inICU = np.zeros(n_steps)
            inICU[step] = poisson.rvs(inICU_ave[step])
        else:
            inICU = inICU_ave

    return inICU
