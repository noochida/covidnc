import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import pickle
from scipy.integrate import solve_ivp
import time
import matplotlib.pyplot as plt
import os
from time import perf_counter
from scipy.stats import nbinom
from multiprocessing import Pool
import sys

plt.ion()

# path to directory
path = ".../COVIDNC"
sys.path.append(path + "/Scripts")

from model import *

# Read vaccination data
dfvac = pd.read_csv(path + "/Data/dfvac.csv", parse_dates=["date"])
dfvac = dfvac.drop(["cumultot", "prct"], axis=1)

dfhosp = pd.read_csv(path + "/Data/dfobs.csv", parse_dates=["date"])

# Define key dates
date_start = pd.to_datetime(dfvac.date[0])
date_intro = pd.to_datetime("2021-08-10")
date_lock = pd.to_datetime("2021-09-07")
date_partial_lift = pd.to_datetime("2021-10-11")
date_full_lift = pd.to_datetime("2021-11-15")
date_sim_end = pd.to_datetime(dfobs.date.max())

# Calculate phase lengths
l1 = (date_intro - date_start).days
l2 = (date_lock - date_intro).days
l3 = (date_partial_lift - date_lock).days
l4 = (date_full_lift - date_partial_lift).days
l5 = (date_sim_end - date_full_lift).days + 1
ls4 = [l1, l2, l3, l4, l5]

# Set up date and time
dates4 = pd.date_range(date_start, date_sim_end)
sim_len4 = len(dates4)
sample_times4 = np.arange(sim_len4)


def switch_eta(t, t1, eta, nu, k=1):
    switch = eta / (1 + np.exp(-k * (t - t1 - nu)))
    return switch


# Process vaccination data and create an interpolation function
matvac4 = np.transpose(dfvac.values[:sim_len4, 1:])
vacFunc4 = interp1d(sample_times4, matvac4, bounds_error=False, fill_value="extrapolate")


# Set initial conditions
n_a = 6  # age groups
n_s = 38  # statuts
y0 = np.zeros([n_a, n_s])
y0[:, 0] = [48039, 16501, 134761, 44684, 16639, 10783]  # S0
y0 = y0.flatten()

hosp_probs = [0.002,
              0.001,
              0.006366023794823,
              0.038815175342405,
              0.088984674679969,
              0.218271736807938]

# Assume delta severity
hosp_probs = np.array([1.5, 1.5, 1.5, 1.5, 1.5, 1.00]) * np.array(hosp_probs)

# Define susceptibility factors
susceps = np.array([(1 - 0.5), (1 - 0.25), 1, 1, 1, 1])

# Define protection factors for infection and severity
inf_vac = 0.40
sus_vac1 = 0.33
sus_vac2 = 0.85
sev_vac1 = 0.85
sev_vac2 = 0.95
protecs = np.array([inf_vac, sus_vac1, sev_vac1, sus_vac2, sev_vac2])

# Set initial value for infection in age group 16.49 and create an interpolation function
i0 = 1
infs = np.zeros((n_a, sim_len4))
infs[3, l1] = i0
infFunc4 = interp1d(sample_times4, infs, bounds_error=False, fill_value="extrapolate")


# Retrieve parameters
with open(path + "/Production/idata_MCMC.pkl", "rb") as f:
    dest = pickle.load(f)

mR0 = dest["R0"]
meta = dest["eta"]
mnu = dest["nu"]
malpha_hosp = dest["alpha_hosp"]
sR0 = np.random.choice(mR0, 1000)
seta = np.random.choice(meta, 1000)
snu = np.random.choice(mnu, 1000)
salpha_hosp = np.random.choice(malpha_hosp, 1000)


with open(path + "/Production/calibration.pkl", "rb+") as f:
    dcalib = pickle.load(f)

eta2 = dcalib["eta2"]
nu2 = dcalib["nu2"]
eta3 = dcalib["eta3"]
nu3 = dcalib["nu3"]


def fbase(R01, eta1, nu1):

    Rnull = 0
    tlock = l1 + l2
    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R01, (l2+l3+l4+l5))))
    switch = switch_eta(sample_times4, t1=tlock,  eta=eta1, nu=nu1)
    Reffs1 = R0s * (1 - switch)

    tpartial_lift = l1 + l2 + l3
    switch2 = switch_eta(sample_times4, t1=tpartial_lift, eta=eta2, nu=nu2)
    Reffs2 = Reffs1 * (1 + switch2)

    tfull_lift = l1 + l2 + l3 + l4
    switch3 = switch_eta(sample_times4, t1=tfull_lift, eta=eta3, nu=nu3)
    Reffs3 = Reffs2 * (1 + switch3)

    # Interpolate the effective reproduction number (Reff) as a function of time
    R0Func4 = interp1d(sample_times4, Reffs3, bounds_error=False, fill_value="extrapolate")

    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len4),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, ls4, infFunc4, vacFunc4, R0Func4),
                    t_eval=sample_times4,
                    method="LSODA")

    age_indices = n_s * np.arange(n_a)

    Icumulflat = np.sum(sol.y[[i + j for i in [7, 16, 25, 34] for j in age_indices], :], axis=0)
    iInf = Icumulflat[1:] - Icumulflat[:-1]
    iInf_simul = np.insert(iInf, 0, Icumulflat[0])

    Hcumulflat = np.sum(sol.y[[i + j for i in [8, 17, 26, 35] for j in age_indices], :], axis=0)
    iHosp = Hcumulflat[1:] - Hcumulflat[:-1]
    iHosp_simul = np.insert(iHosp, 0, Hcumulflat[0])

    return iInf_simul, iHosp_simul


def sim(i, sR0, seta, snu, salpha_hosp):
    R0i = sR0[i]
    etai = seta[i]
    nui = snu[i]
    alpha_hospi = salpha_hosp[i]

    inf, hosp = fbase(R01=R0i, eta1=etai, nu1=nui)

    # Generator
    ni = 1/alpha_hospi
    mui = hosp
    pi = mui / (mui + ni)
    hosp_nbin = nbinom.rvs(n=ni, p=(1 - pi))

    return inf, hosp_nbin


start = time.perf_counter()

with Pool(16) as p:
    results = p.starmap(sim, [(i, sR0, seta, snu, salpha_hosp) for i in range(len(sR0))])

infs, hosps = zip(*results)
infs_base, hosps_base = np.array(infs), np.array(hosps)

print("base sim:")
print(f"Completed Execution in {(time.perf_counter() - start)/60} minutes")


################################
# No vaccination after discovery
################################
tlock = l1 + l2

# Process vaccination data and create an interpolation function
matvac4 = np.transpose(dfvac.values[:sim_len4, 1:])
matvac_partial = np.zeros_like(matvac4)
matvac_partial[:, :tlock] = matvac4[:, :tlock]

vacFunc_partial = interp1d(sample_times4, matvac_partial, bounds_error=False, fill_value="extrapolate")


def fvac_partial(R01, eta1, nu1):

    Rnull = 0
    tlock = l1 + l2

    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R01, (l2+l3+l4+l5))))
    switch = switch_eta(sample_times4, t1=tlock,  eta=eta1, nu=nu1)
    Reffs1 = R0s * (1 - switch)

    tpartial_lift = l1 + l2 + l3
    switch2 = switch_eta(sample_times4, t1=tpartial_lift, eta=eta2, nu=nu2)
    Reffs2 = Reffs1 * (1 + switch2)

    tfull_lift = l1 + l2 + l3 + l4
    switch3 = switch_eta(sample_times4, t1=tfull_lift, eta=eta3, nu=nu3)
    Reffs3 = Reffs2 * (1 + switch3)

    R0Func4 = interp1d(sample_times4, Reffs3, bounds_error=False, fill_value="extrapolate")

    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len4),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, ls4, infFunc4, vacFunc_partial, R0Func4),
                    t_eval=sample_times4,
                    method="LSODA")

    age_indices = n_s * np.arange(n_a)

    Icumulflat = np.sum(sol.y[[i + j for i in [7, 16, 25, 34] for j in age_indices], :], axis=0)
    iInf = Icumulflat[1:] - Icumulflat[:-1]
    iInf_simul = np.insert(iInf, 0, Icumulflat[0])
    # iInf_simul = iInf_simul[inds_obs4]

    Hcumulflat = np.sum(sol.y[[i + j for i in [8, 17, 26, 35] for j in age_indices], :], axis=0)
    iHosp = Hcumulflat[1:] - Hcumulflat[:-1]
    iHosp_simul = np.insert(iHosp, 0, Hcumulflat[0])
    # iHosp_simul = iHosp_simul[inds_obs4]

    return iInf_simul, iHosp_simul


def sim_vac_partial(i, sR0, seta, snu, salpha_hosp):
    R0i = sR0[i]
    etai = seta[i]
    nui = snu[i]
    alpha_hospi = salpha_hosp[i]

    inf, hosp = fvac_partial(R01=R0i, eta1=etai, nu1=nui)

    # Generator
    ni = 1 / alpha_hospi
    mui = hosp
    pi = mui / (mui + ni)
    hosp_nbin = nbinom.rvs(n=ni, p=(1 - pi))

    return inf, hosp_nbin


start = time.perf_counter()

with Pool(16) as p:
    results = p.starmap(sim_vac_partial, [(i, sR0, seta, snu, salpha_hosp) for i in range(len(sR0))])

print("Vac partial:")
print(f"Completed Execution in {(time.perf_counter() - start)/60} minutes")

infs, hosps = zip(*results)

infs_vac_partial, hosps_vac_partial = np.array(infs), np.array(hosps)


# No lockdown
def fno_lock(R01):

    Rnull = 0

    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R01, (l2+l3+l4+l5))))
    R0Func4 = interp1d(sample_times4, R0s, bounds_error=False, fill_value="extrapolate")

    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len4),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, ls4, infFunc4, vacFunc4, R0Func4),
                    t_eval=sample_times4,
                    method="LSODA")

    age_indices = n_s * np.arange(n_a)

    Icumulflat = np.sum(sol.y[[i + j for i in [7, 16, 25, 34] for j in age_indices], :], axis=0)
    iInf = Icumulflat[1:] - Icumulflat[:-1]
    iInf_simul = np.insert(iInf, 0, Icumulflat[0])

    Hcumulflat = np.sum(sol.y[[i + j for i in [8, 17, 26, 35] for j in age_indices], :], axis=0)
    iHosp = Hcumulflat[1:] - Hcumulflat[:-1]
    iHosp_simul = np.insert(iHosp, 0, Hcumulflat[0])

    return iInf_simul, iHosp_simul


def sim_nolock(i, sR0, salpha_hosp):

    R0i = sR0[i]
    alpha_hospi = salpha_hosp[i]

    inf, hosp = fno_lock(R01=R0i)

    # Generator
    ni = 1 / alpha_hospi
    mui = hosp
    pi = mui / (mui + ni)
    hosp_nbin = nbinom.rvs(n=ni, p=(1 - pi))

    return inf, hosp_nbin


start = time.perf_counter()

with Pool(16) as p:
    results = p.starmap(sim_nolock, [(i, sR0, salpha_hosp) for i in range(len(sR0))])

print("No lockdown:")
print(f"Completed Execution in {(time.perf_counter() - start)/60} minutes")

infs, hosps = zip(*results)
infs_nolock, hosps_nolock = np.array(infs), np.array(hosps)


# Découverte plus tôt
date_lock15 = pd.to_datetime("2021-09-07") - pd.Timedelta(15, "D")
date_partial_lift15 = pd.to_datetime("2021-10-11") - pd.Timedelta(15, "D")
date_full_lift15 = pd.to_datetime("2021-11-15") - pd.Timedelta(15, "D")

l2_15 = (date_lock15 - date_intro).days
l3_15 = (date_partial_lift15 - date_lock15).days
l4_15 = (date_full_lift15 - date_partial_lift15).days
l5_15 = (date_sim_end - date_full_lift15).days + 1

ls15 = [l1, l2_15, l3_15, l4_15, l5_15]
sim_len15 = np.sum(ls15)


def fsooner(R01, eta1, nu1):

    Rnull = 0
    tlock15 = l1 + l2_15
    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R01, (l2_15+l3_15+l4_15+l5_15))))
    switch = switch_eta(sample_times4, t1=tlock15,  eta=eta1, nu=nu1)
    Reffs1 = R0s * (1 - switch)

    tpartial_lift15 = l1 + l2_15 + l3_15
    switch2 = switch_eta(sample_times4, t1=tpartial_lift15, eta=eta2, nu=nu2)
    Reffs2 = Reffs1 * (1 + switch2)

    tfull_lift15 = l1 + l2_15 + l3_15 + l4_15
    switch3 = switch_eta(sample_times4, t1=tfull_lift15, eta=eta3, nu=nu3)
    Reffs3 = Reffs2 * (1 + switch3)

    # Interpolate the effective reproduction number (Reff) as a function of time
    R0Func4 = interp1d(sample_times4, Reffs3, bounds_error=False, fill_value="extrapolate")

    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len15),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, ls4, infFunc4, vacFunc4, R0Func4),
                    t_eval=sample_times4,
                    method="LSODA")

    age_indices = n_s * np.arange(n_a)

    Icumulflat = np.sum(sol.y[[i + j for i in [7, 16, 25, 34] for j in age_indices], :], axis=0)
    iInf = Icumulflat[1:] - Icumulflat[:-1]
    iInf_simul = np.insert(iInf, 0, Icumulflat[0])

    Hcumulflat = np.sum(sol.y[[i + j for i in [8, 17, 26, 35] for j in age_indices], :], axis=0)
    iHosp = Hcumulflat[1:] - Hcumulflat[:-1]
    iHosp_simul = np.insert(iHosp, 0, Hcumulflat[0])

    return iInf_simul, iHosp_simul


def sim_sooner(i, sR0, seta, snu, salpha_hosp):
    R0i = sR0[i]
    etai = seta[i]
    nui = snu[i]
    alpha_hospi = salpha_hosp[i]

    inf, hosp = fsooner(R01=R0i, eta1=etai, nu1=nui)

    # Generator
    ni = 1 / alpha_hospi
    mui = hosp
    pi = mui / (mui + ni)
    hosp_nbin = nbinom.rvs(n=ni, p=(1 - pi))

    return inf, hosp_nbin


start = time.perf_counter()

with Pool(16) as p:
    results = p.starmap(sim_sooner, [(i, sR0, seta, snu, salpha_hosp) for i in range(len(sR0))])

print("Sooner:")
print(f"Completed Execution in {(time.perf_counter() - start)/60} minutes")

infs, hosps = zip(*results)

infs_sooner, hosps_sooner = np.array(infs), np.array(hosps)


dres = {
    "date": dates4,
    "infs_base": infs_base,
    "hosps_base": hosps_base,
    "infs_vac_partial": infs_vac_partial,
    "hosps_vac_partial": hosps_vac_partial,
    "infs_nolock": infs_nolock,
    "hosps_nolock": hosps_nolock,
    "infs_sooner": infs_sooner,
    "hosps_sooner": hosps_sooner
    }

with open(path + "/Production/scenarios.pkl", "wb") as f:
    pickle.dump(dres, f)
