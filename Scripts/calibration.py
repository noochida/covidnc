import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import pickle
from scipy.integrate import solve_ivp
from scipy.optimize import curve_fit
import time
import matplotlib.pyplot as plt
import os
from time import perf_counter
plt.ion()

# path to directory
path = ".../COVIDNC"
sys.path.append(path + "/Scripts")

from model import *


# Read vaccination data
dfvac = pd.read_csv(path + "/Data/dfvac.csv", parse_dates=["date"])
dfvac = dfvac.drop(["cumultot", "prct"], axis=1)

dfhosp = pd.read_csv(path + "/Data/dfobs.csv", parse_dates=["date"])

# Define key dates
date_start = pd.to_datetime(dfvac.date[0])
date_intro = pd.to_datetime("2021-08-10")
date_lock = pd.to_datetime("2021-09-07")
date_part_lift = pd.to_datetime("2021-10-11")
date_sim_end = pd.to_datetime(date_part_lift)

# Calculate phase lengths
l1 = (date_intro - date_start).days
l2 = (date_lock - date_intro).days
l3 = (date_part_lift - date_lock).days
ls = [l1, l2, l3]

# Set up date and time
dates = pd.date_range(date_start, date_sim_end, inclusive="left")
sim_len = len(dates)
sample_times = np.arange(sim_len)
# Set up observations
obs_start = pd.to_datetime("2021-09-06")
dfobs = dfobs.loc[(dfobs.date >= obs_start) & (dfobs.date < date_sim_end)].copy()
inds_obs = np.where(dates.isin(dfobs.date))[0]
hosp_obs = dfobs.hosp.values

# Process vaccination data and create an interpolation function
matvac = np.transpose(dfvac.values[:sim_len, 1:])
vacFunc = interp1d(sample_times, matvac, bounds_error=False, fill_value="extrapolate")

# Set initial conditions
n_a = 6  # age groups
n_s = 38  # statuts
y0 = np.zeros([n_a, n_s])
y0[:, 0] = [48039, 16501, 134761, 44684, 16639, 10783]  # S0
y0 = y0.flatten()

hosp_probs = [0.002,
              0.001,
              0.006366023794823,
              0.038815175342405,
              0.088984674679969,
              0.218271736807938]


# Assume delta severity
hosp_probs = np.array([1.5, 1.5, 1.5, 1.5, 1.5, 1.00]) * np.array(hosp_probs)


# Define susceptibility factors
susceps = np.array([(1 - 0.5), (1 - 0.25), 1, 1, 1, 1])

# Define protection factors for infection and severity
inf_vac = 0.40
sus_vac1 = 0.33
sus_vac2 = 0.85
sev_vac1 = 0.85
sev_vac2 = 0.95
protecs = np.array([inf_vac, sus_vac1, sev_vac1, sus_vac2, sev_vac2])

# Set initial value for infection in age group 16.49 and create an interpolation function
i0 = 1
infs = np.zeros((n_a, sim_len))
infs[3, l1] = i0
infFunc = interp1d(sample_times, infs, bounds_error=False, fill_value="extrapolate")


#################################
# After partial lift calibration
#################################

# Define key dates
date_full_lift = pd.to_datetime("2021-11-15")
date_sim_end = pd.to_datetime(date_full_lift)

# Calculate phase lengths
l3 = (date_part_lift - date_lock).days
l4 = (date_full_lift - date_part_lift).days
ls2 = [l1, l2, l3, l4]

# Set up date and time
dates2 = pd.date_range(date_start, date_sim_end, inclusive="left")
sim_len2 = len(dates2)
sample_times2 = np.arange(sim_len2)

# setup observations
dfobs2 = dfobs.loc[(dfobsp.date >= obs_start) & (dfobs.date < date_sim_end)].copy()
inds_obs2 = np.where(dates2.isin(dfobs2.date))[0]
hosp_obs2 = dfobs2.hosp.values


# Process vaccination data and create an interpolation function
matvac2 = np.transpose(dfvac.values[:sim_len2, 1:])
vacFunc2 = interp1d(sample_times2, matvac2, bounds_error=False, fill_value="extrapolate")

# Set initial value for infection in age group 16.49 and create an interpolation function
i0 = 1
infs = np.zeros((n_a, sim_len2))
infs[3, l1] = i0
infFunc2 = interp1d(sample_times2, infs, bounds_error=False, fill_value="extrapolate")

# Extraction of parameters posterior distribution
with open(path + "/Production/idata_MCMC.pkl", "rb") as f:
    destim = pickle.load(f)

R01 = np.mean(destim["R0"])
eta1 = np.mean(destim["eta"])
nu1 = np.mean(destim["nu"])

# Output dictionnary
dcalib = {"R01": R01, "eta1": eta1, "nu1": nu1}


def my_model2(t, eta2, nu2):

    Rnull = 0
    tlock = l1 + l2
    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R01, (l2+l3+l4))))
    switch = switch_eta(sample_times2, t1=tlock,  eta=eta1, nu=nu1)
    Reffs1 = R0s * (1 - switch)

    tpartial_lift = l1 + l2 + l3
    switch2 = switch_eta(sample_times2, t1=tpartial_lift, eta=eta2, nu=nu2)
    Reffs2 = Reffs1 * (1 + switch2)

    # Interpolate the effective reproduction number (Reff) as a function of time
    R0Func2 = interp1d(sample_times2, Reffs2, bounds_error=False, fill_value="extrapolate")
    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len2),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, ls2, infFunc2, vacFunc2, R0Func2),
                    t_eval=sample_times2,
                    method="LSODA")

    age_indices = n_s * np.arange(n_a)
    Hcumulflat = np.sum(sol.y[[i + j for i in [8, 17, 26, 35] for j in age_indices], :], axis=0)
    iHosp = Hcumulflat[1:] - Hcumulflat[:-1]
    iHosp_simul = np.insert(iHosp, 0, Hcumulflat[0])
    iHosp_simul = iHosp_simul[inds_obs2]

    return iHosp_simul


start = time.perf_counter()
popt2, cov2 = curve_fit(my_model2, sample_times2, hosp_obs2, p0=[0.5, 6], bounds=([0, 3], [3, 15]))
print(f"Completed Execution in {time.perf_counter() - start} seconds")

dcalib["eta2"] = popt2[0]
dcalib["nu2"] = popt2[1]

##############################
# After full lift calibration
##############################
# Define key dates
date_sim_end = pd.to_datetime(dfhosp.date.max())
# Calculate phase lengths
l4 = (date_full_lift - date_part_lift).days
l5 = (date_sim_end - date_full_lift).days + 1
ls3 = [l1, l2, l3, l4, l5]

# Set up date and time
dates3 = pd.date_range(date_start, date_sim_end)
sim_len3 = len(dates3)
sample_times3 = np.arange(sim_len3)

# setup observations
dfobs3 = dfobs.loc[(dfobs.date >= obs_start) & (dfobs.date <= date_sim_end)].copy()
inds_obs3 = np.where(dates3.isin(dfobs3.date))[0]
hosp_obs3 = dfobs3.hosp.values

# Process vaccination data and create an interpolation function
matvac3 = np.transpose(dfvac.values[:sim_len3, 1:])
vacFunc3 = interp1d(sample_times3, matvac3, bounds_error=False, fill_value="extrapolate")

# Set initial value for infection in age group 16.49 and create an interpolation function
i0 = 1
infs = np.zeros((n_a, sim_len3))
infs[3, l1] = i0
infFunc3 = interp1d(sample_times3, infs, bounds_error=False, fill_value="extrapolate")

# # Uncover parameters
R01 = dcalib["R01"]
eta1 = dcalib["eta1"]
nu1 = dcalib["nu1"]
eta2 = dcalib["eta2"]
nu2 = dcalib["nu2"]


def my_model3(t, eta3, nu3):

    Rnull = 0
    tlock = l1 + l2
    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R01, (l2+l3+l4+l5))))
    switch = switch_eta(sample_times3, t1=tlock,  eta=eta1, nu=nu1)
    Reffs1 = R0s * (1 - switch)

    tpartial_lift = l1 + l2 + l3
    switch2 = switch_eta(sample_times3, t1=tpartial_lift, eta=eta2, nu=nu2)
    Reffs2 = Reffs1 * (1 + switch2)

    tfull_lift = l1 + l2 + l3 + l4
    switch3 = switch_eta(sample_times3, t1=tfull_lift, eta=eta3, nu=nu3)

    Reffs3 = Reffs2 * (1 + switch3)
    # Interpolate the effective reproduction number (Reff) as a function of time
    R0Func3 = interp1d(sample_times3, Reffs3, bounds_error=False, fill_value="extrapolate")

    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len3),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, ls3, infFunc3, vacFunc3, R0Func3),
                    t_eval=sample_times3,
                    method="LSODA")

    age_indices = n_s * np.arange(n_a)
    Hcumulflat = np.sum(sol.y[[i + j for i in [8, 17, 26, 35] for j in age_indices], :], axis=0)
    iHosp = Hcumulflat[1:] - Hcumulflat[:-1]
    iHosp_simul = np.insert(iHosp, 0, Hcumulflat[0])
    iHosp_simul = iHosp_simul[inds_obs3]

    return iHosp_simul


start = time.perf_counter()
popt3, cov3 = curve_fit(my_model3, sample_times3, hosp_obs3, p0=[0.5, 6], bounds=([0, 3], [3, 15]))
print(f"Completed Execution in {time.perf_counter() - start} seconds")

dcalib["eta3"] = popt3[0]
dcalib["nu3"] = popt3[1]

with open(path + "/Production/calibration.pkl", "wb") as f3:
    pickle.dump(dcalib, f3)
