import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import pickle
from scipy.integrate import solve_ivp
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import os
from time import perf_counter
from scipy.stats import nbinom
from multiprocessing import Pool
import sys

plt.ion()

# path to directory
path = ".../COVIDNC"
sys.path.append(path + "/Scripts")

from model import *

# Read vaccination data
dfvac = pd.read_csv(path + "/Data/dfvac.csv", parse_dates=["date"])
dfvac = dfvac.drop(["cumultot", "prct"], axis=1)

dfobs = pd.read_csv(path + "/Data/dfobs.csv", parse_dates=["date"])

# Define key dates
date_start = pd.to_datetime(dfvac.date[0])
date_intro = pd.to_datetime("2021-08-10")
date_lock = pd.to_datetime("2021-09-07")
date_partial_lift = pd.to_datetime("2021-10-11")
date_full_lift = pd.to_datetime("2021-11-15")
date_sim_end = pd.to_datetime(dfobs.date.max())

# Calculate phase lengths
l1 = (date_intro - date_start).days
l2 = (date_lock - date_intro).days
l3 = (date_partial_lift - date_lock).days
l4 = (date_full_lift - date_partial_lift).days
l5 = (date_sim_end - date_full_lift).days + 1
ls4 = [l1, l2, l3, l4, l5]

# Set up date and time
dates4 = pd.date_range(date_start, date_sim_end)
sim_len4 = len(dates4)
sample_times4 = np.arange(sim_len4)


def switch_eta(t, t1, eta, nu, k=1):
    switch = eta / (1 + np.exp(-k * (t - t1 - nu)))
    return switch


# Process vaccination data and create an interpolation function
matvac4 = np.transpose(dfvac.values[:sim_len4, 1:])
vacFunc4 = interp1d(sample_times4, matvac4, bounds_error=False, fill_value="extrapolate")


# Set initial conditions
n_a = 6  # age groups
n_s = 38  # statuts
y0 = np.zeros([n_a, n_s])
y0[:, 0] = [48039, 16501, 134761, 44684, 16639, 10783]  # S0
y0 = y0.flatten()

hosp_probs = [0.002,
              0.001,
              0.006366023794823,
              0.038815175342405,
              0.088984674679969,
              0.218271736807938]

# Assume delta severity
hosp_probs = np.array([1.5, 1.5, 1.5, 1.5, 1.5, 1.00]) * np.array(hosp_probs)
# hosp_probs = 1.5 * np.array(hosp_probs)


# Define susceptibility factors
susceps = np.array([(1 - 0.5), (1 - 0.25), 1, 1, 1, 1])

# Define protection factors for infection and severity
inf_vac = 0.40
sus_vac1 = 0.33
sus_vac2 = 0.85
sev_vac1 = 0.85
sev_vac2 = 0.95
protecs = np.array([inf_vac, sus_vac1, sev_vac1, sus_vac2, sev_vac2])

# Set initial value for infection in age group 16.49 and create an interpolation function
i0 = 1
infs = np.zeros((n_a, sim_len4))
infs[3, l1] = i0
infFunc4 = interp1d(sample_times4, infs, bounds_error=False, fill_value="extrapolate")


def fbase(R01, eta1, nu1):

    Rnull = 0
    tlock = l1 + l2
    R0s = np.concatenate((np.repeat(Rnull, l1), np.repeat(R01, (l2+l3+l4+l5))))
    switch = switch_eta(sample_times4, t1=tlock,  eta=eta1, nu=nu1)
    Reffs1 = R0s * (1 - switch)

    tpartial_lift = l1 + l2 + l3
    switch2 = switch_eta(sample_times4, t1=tpartial_lift, eta=eta2, nu=nu2)
    Reffs2 = Reffs1 * (1 + switch2)

    tfull_lift = l1 + l2 + l3 + l4
    switch3 = switch_eta(sample_times4, t1=tfull_lift, eta=eta3, nu=nu3)
    Reffs3 = Reffs2 * (1 + switch3)

    # Interpolate the effective reproduction number (Reff) as a function of time
    R0Func4 = interp1d(sample_times4, Reffs3, bounds_error=False, fill_value="extrapolate")

    sol = solve_ivp(SEIR,
                    t_span=(0, sim_len4),
                    y0=y0,
                    args=(hosp_probs, susceps, protecs, ls4, infFunc4, vacFunc4, R0Func4),
                    t_eval=sample_times4,
                    method="LSODA")

    return sol


with open(path + "/Production/calibration.pkl", "rb+") as f:
    dcalib = pickle.load(f)

R01 = dcalib["R01"]
eta1 = dcalib["eta1"]
nu1 = dcalib["nu1"]
eta2 = dcalib["eta2"]
nu2 = dcalib["nu2"]
eta3 = dcalib["eta3"]
nu3 = dcalib["nu3"]

t1 = perf_counter()
sol = fbase(R01=R01, eta1=eta1, nu1=nu1)
t2 = perf_counter()
# time tracking
s = t2 - t1
print(f"Elapsed time : {s:.8f}s")

y = sol["y"]
age_indices = n_s * np.arange(n_a)

S, E1, E2, Imild, Ihosp, H, R, Icumul, Hcumul, \
    SV0, E1V0, E2V0, ImildV0, IhospV0, HV0, RV0, IcumulV0, HcumulV0, \
    SV1, E1V1, E2V1, ImildV1, IhospV1, HV1, RV1, IcumulV1, HcumulV1, \
    SV2, E1V2, E2V2, ImildV2, IhospV2, HV2, RV2, IcumulV2, HcumulV2,\
    SV0cumul, SV2cumul = y[[i + age_indices for i in np.arange(n_s)]]

# Hospitalisation non vac

iH = Hcumul[:, 1:] - Hcumul[:, :-1]
iH = np.insert(iH, 0, Hcumul[:, 0], axis=1)
iHtot = np.sum(iH, axis=1)

iHV0 = HcumulV0[:, 1:] - HcumulV0[:, :-1]
iHV0 = np.insert(iHV0, 0, HcumulV0[:, 0], axis=1)
iHV0tot = np.sum(iHV0, axis=1)

iHV1 = HcumulV1[:, 1:] - HcumulV1[:, :-1]
iHV1 = np.insert(iHV1, 0, HcumulV1[:, 0], axis=1)
iHV1tot = np.sum(iHV1, axis=1)

iHV2 = HcumulV2[:, 1:] - HcumulV2[:, :-1]
iHV2 = np.insert(iHV2, 0, HcumulV2[:, 0], axis=1)
iHV2tot = np.sum(iHV2, axis=1)

print(np.sum([iHtot, iHV0tot, iHV1tot, iHV2tot]))

iI = Icumul[:, 1:] - Icumul[:, :-1]
iI = np.insert(iI, 0, Icumul[:, 0], axis=1)
iItot = np.sum(iI, axis=1)

iIV0 = IcumulV0[:, 1:] - IcumulV0[:, :-1]
iIV0 = np.insert(iIV0, 0, IcumulV0[:, 0], axis=1)
iIV0tot = np.sum(iIV0, axis=1)

iIV1 = IcumulV1[:, 1:] - IcumulV1[:, :-1]
iIV1 = np.insert(iIV1, 0, IcumulV1[:, 0], axis=1)
iIV1tot = np.sum(iIV1, axis=1)

iIV2 = IcumulV2[:, 1:] - IcumulV2[:, :-1]
iIV2 = np.insert(iIV2, 0, IcumulV2[:, 0], axis=1)
iIV2tot = np.sum(iIV2, axis=1)

Ivac = SV0cumul[:, 1:] - SV0cumul[:, :-1]
Ivac = np.insert(Ivac, 0, SV0cumul[:, 0], axis=1)

# Infected
infs = np.vstack([iItot, iIV0tot, iIV1tot, iIV2tot])

# Combine the arrays into a single 2D array
hosps = np.vstack([iHtot, iHV0tot, iHV1tot, iHV2tot])

plt.style.use("seaborn-whitegrid")

# Define age groups and status labels
age_groups = ['0-11', '12-15', '16-49', '50-64', '65-74', '75+']
status_labels = ['Non vaccinated', '1st dose before protection',
                 '1st level of protection', '2nd level of protection']

colors_hosps = ["#D3E6FF", "#A6C9FF", "#78ACFF", "#508FFF"]
colors_infs = ["#FFEBEB", "#FFC9C9", "#FFA8A8", "#FF8787"]

plt.style.use("seaborn-whitegrid")
fig, (ax1, ax2) = plt.subplots(figsize=(6, 5), nrows=1, ncols=2)
plt.rcParams.update({'font.size': 9})

bar_width = 0.15
x = np.arange(len(age_groups))

for i, (status, values, color) in enumerate(zip(status_labels, infs, colors_infs)):
    ax1.bar(x - (bar_width * (len(status_labels) / 2) - i * bar_width), values,
            width=bar_width, label=status, color=color, edgecolor="darkgray")

for i, (status, values, color) in enumerate(zip(status_labels, hosps, colors_hosps)):
    ax2.bar(x - (bar_width * (len(status_labels) / 2) - i * bar_width), values,
            width=bar_width, label=status, color=color, edgecolor="darkgray")

ax1.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))
ax1.set_axisbelow(True)
ax1.grid(which='major', axis='y', color='#666666')
ax1.grid(which='minor', axis='y', color='#999999', alpha=0.2)

ax1.set_xticks(x)
ax1.set_xticklabels(age_groups)
ax1.set_ylim([-0.1, 40000])
ax1.legend(fontsize="x-small", frameon=True)

ax2.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))
ax2.set_axisbelow(True)
ax2.grid(which='major', axis='y', color='#666666')
ax2.grid(which='minor', axis='y', color='#999999', alpha=0.2)

ax2.set_xticks(x)
ax2.set_xticklabels(age_groups)
ax2.set_ylim([-0.5, 400])
ax2.legend(fontsize="x-small", frameon=True)

ax1.annotate("A", (0.01, 0.96), fontsize=12,
             xycoords="axes fraction", weight="bold")
ax2.annotate("B", (0.01, 0.96), fontsize=12,
             xycoords="axes fraction", weight="bold")

fig.supxlabel("Age Group", y=0.001)
# fig.suptitle("Infected by Age Group and Vaccination Status")
plt.subplots_adjust(top=0.96, bottom=0.07, left=0.07, right=0.98,
                    wspace=0.2)

plt.savefig(path + "/Figures/Review/FigSup1.pdf", format="pdf")
plt.close()


# Extract values

# Outputs
with open(path + "/Production/dsims.pkl", "rb") as f:
    dsims = pickle.load(f)

dates_sim = dsims["date"]
date_obs_sim = dsims["date_obs"]

# base
infs = dsims["infs_base"]
minfs = np.mean(dsims["infs_base"], axis=0)

hosps = dsims["hosps_base"]
hosps = hosps[:, np.isin(dates_sim, date_obs_sim)]
qhosps = np.quantile(hosps, q=[0.025, 0.975], axis=0)
mhosps = np.mean(hosps, axis=0)

icu = dsims["icu_base"]
qicu = np.quantile(icu, q=[0.025, 0.975], axis=0)
micu = np.mean(icu, axis=0)

icu_alt = dsims["icu_base_alt"]
qicu_alt = np.quantile(icu_alt, q=[0.025, 0.975], axis=0)
micu_alt = np.mean(icu_alt, axis=0)

occ_icu = dsims["occ_icu_base"]
qocc_icu = np.quantile(occ_icu, q=[0.025, 0.975], axis=0)
mocc_icu = np.mean(occ_icu, axis=0)

occ_icu_alt = dsims["occ_icu_base_alt"]
qocc_icu_alt = np.quantile(occ_icu_alt, q=[0.025, 0.975], axis=0)
mocc_icu_alt = np.mean(occ_icu_alt, axis=0)

# Partial vaccination
infs_vac_partial = dsims["infs_vac_partial"]
minfs_vac_partial = np.mean(dsims["infs_vac_partial"], axis=0)

hosps_vac_partial = dsims["hosps_vac_partial"]
hosps_vac_partial = hosps_vac_partial[:, np.isin(dates_sim, date_obs_sim)]
qhosps_vac_partial = np.quantile(hosps_vac_partial, q=[0.025, 0.975], axis=0)
mhosps_vac_partial = np.mean(hosps_vac_partial, axis=0)

icu_vac_partial = dsims["icu_vac_partial"]
qicu_vac_partial = np.quantile(icu_vac_partial, q=[0.025, 0.975], axis=0)
micu_vac_partial = np.mean(icu_vac_partial, axis=0)

icu_vac_partial_alt = dsims["icu_vac_partial_alt"]
qicu_vac_partial_alt = np.quantile(icu_vac_partial_alt, q=[0.025, 0.975], axis=0)
micu_vac_partial_alt = np.mean(icu_vac_partial_alt, axis=0)

occ_icu_vac_partial = dsims["occ_icu_vac_partial"]
qocc_icu_vac_partial = np.quantile(occ_icu_vac_partial, q=[0.025, 0.975], axis=0)
mocc_icu_vac_partial = np.mean(occ_icu_vac_partial, axis=0)

occ_icu_vac_partial_alt = dsims["occ_icu_vac_partial_alt"]
qocc_icu_vac_partial_alt = np.quantile(occ_icu_vac_partial_alt, q=[0.025, 0.975], axis=0)
mocc_icu_vac_partial_alt = np.mean(occ_icu_vac_partial_alt, axis=0)

# No lockdwon
infs_nolock = dsims["infs_nolock"]
minfs_nolock = np.mean(dsims["infs_nolock"], axis=0)

hosps_nolock = dsims["hosps_nolock"]
hosps_nolock = hosps_nolock[:, np.isin(dates_sim, date_obs_sim)]
qhosps_nolock = np.quantile(hosps_nolock, q=[0.025, 0.975], axis=0)
mhosps_nolock = np.mean(hosps_nolock, axis=0)

icu_nolock = dsims["icu_nolock"]
qicu_nolock = np.quantile(icu_nolock, q=[0.025, 0.975], axis=0)
micu_nolock = np.mean(icu_nolock, axis=0)

icu_nolock_alt = dsims["icu_nolock_alt"]
qicu_nolock_alt = np.quantile(icu_nolock_alt, q=[0.025, 0.975], axis=0)
micu_nolock_alt = np.mean(icu_nolock_alt, axis=0)

occ_icu_nolock = dsims["occ_icu_nolock"]
qocc_icu_nolock = np.quantile(occ_icu_nolock, q=[0.025, 0.975], axis=0)
mocc_icu_nolock = np.mean(occ_icu_nolock, axis=0)

occ_icu_nolock_alt = dsims["occ_icu_nolock_alt"]
qocc_icu_nolock_alt = np.quantile(occ_icu_nolock_alt, q=[0.025, 0.975], axis=0)
mocc_icu_nolock_alt = np.mean(occ_icu_nolock_alt, axis=0)

# Sooner
infs_sooner = dsims["infs_sooner"]
minfs_sooner = np.mean(dsims["infs_sooner"], axis=0)

hosps_sooner = dsims["hosps_sooner"]
hosps_sooner = hosps_sooner[:, np.isin(dates_sim, date_obs_sim)]
qhosps_sooner = np.quantile(hosps_sooner, q=[0.025, 0.975], axis=0)
mhosps_sooner = np.mean(hosps_sooner, axis=0)

icu_sooner = dsims["icu_sooner"]
qicu_sooner = np.quantile(icu_sooner, q=[0.025, 0.975], axis=0)
micu_sooner = np.mean(icu_sooner, axis=0)

icu_sooner_alt = dsims["icu_sooner_alt"]
qicu_sooner_alt = np.quantile(icu_sooner_alt, q=[0.025, 0.975], axis=0)
micu_sooner_alt = np.mean(icu_sooner_alt, axis=0)

occ_icu_sooner = dsims["occ_icu_sooner"]
qocc_icu_sooner = np.quantile(occ_icu_sooner, q=[0.025, 0.975], axis=0)
mocc_icu_sooner = np.mean(occ_icu_sooner, axis=0)

occ_icu_sooner_alt = dsims["occ_icu_sooner_alt"]
qocc_icu_sooner_alt = np.quantile(occ_icu_sooner_alt, q=[0.025, 0.975], axis=0)
mocc_icu_sooner_alt = np.mean(occ_icu_sooner_alt, axis=0)


dic_tot = {}

# Baseline scenario [pICU fix]
b1 = np.round(np.sum(mhosps))
b2 = np.round(np.max(mocc_icu_alt))
print("Baseline [pICU fixe]")
print(f"{b1} hospital admissions")
print(f"{b2} peak ICU occupancy")

# vacnoconf
_ = np.round(np.sum(mhosps_nolock))
__ = np.round(np.max(mocc_icu_nolock_alt))

print("Vaccination no lockdown:")
print(f"{_} hospital admissions ({np.round(100*(_-b1)/b1)}%)")
print(f"{__} peak ICU occupancy ({np.round(100*(__-b2)/b2)}%)")

# confnovac
_ = np.round(np.sum(mhosps_vac_partial))
__ = np.round(np.max(mocc_icu_vac_partial_alt))

print("Lockdown no vaccination:")
print(f"{_} hospital admissions {np.round(100*(_-b1)/b1)}%)")
print(f"{__} peak ICU occupancy ({np.round(100*(__-b2)/b2)}%)")

# sooner
_ = np.round(np.sum(mhosps_sooner))
__ = np.round(np.max(mocc_icu_sooner_alt))

print("15 day earlier lockdown:")
print(f"{_} hospital admissions ({np.round(100*(_-b1)/b1)}%)")
print(f"{__} peak ICU occupancy ({np.round(100*(__-b2)/b2)}%)")


# Number of vaccination during lockdown

dfvacobs = pd.read_csv(path + "/Data/dfvacobs2023.csv", parse_dates=["date"])

vac_lockdown = (dfvacobs.loc[dfvacobs.date == "2021-10-11", "cumultot"].values
                - dfvacobs.loc[dfvacobs.date == "2021-09-06", "cumultot"].values)[0]

prct_vac_lockdown = (dfvacobs.loc[dfvacobs.date == "2021-10-11", "prct"].values
                     - dfvacobs.loc[dfvacobs.date == "2021-09-06", "prct"].values)[0]
print(f"vaccination pendant le confinement: {np.round(vac_lockdown)}")
print(f"{np.round(prct_vac_lockdown, 2)}% de la population éligible")

vaccination_one_month = (dfvac.loc[dfvac.date == "2021-10-11", "prct"].values
                         - dfvac.loc[dfvac.date == "2021-09-06", "prct"].values)[0]

print(f"{vaccination_one_month} vaccinations during one month of lockdow !")

with open(path + "/Production/idata_MCMC.pkl", "rb") as f:
    destim = pickle.load(f)

_ = np.round(np.mean(destim["R0"]), 1)
__ = np.round(np.quantile(destim["R0"], [0.025, 0.975]), 1)
print(f" R0 = {_}, (95% CI [{__[0]}–{__[1]})")

_ = np.round(np.mean(destim["eta"]), 2)
__ = np.round(np.quantile(destim["eta"], [0.025, 0.975]), 2)
print(f" eta = {_}, (95% CI [{__[0]}–{__[1]})")

_ = np.round(np.mean(destim["nu"]), 1)
__ = np.round(np.quantile(destim["nu"], [0.025, 0.975]), 1)
print(f" nu = {_}, (95% CI [{__[0]}–{__[1]})")

_ = np.round(np.mean(destim["alpha_hosp"]), 4)
__ = np.round(np.quantile(destim["alpha_hosp"], [0.025, 0.975]), 4)
print(f" alpha_hosp = {_}, (95% CI [{__[0]}–{__[1]})")


with open(path + "/Production/calibration.pkl", "rb+") as f:
    dcalib = pickle.load(f)

eta2 = dcalib["eta2"]
nu2 = dcalib["nu2"]

eta3 = dcalib["eta3"]
nu3 = dcalib["nu3"]

print(f"eta2 = {eta2}")
print(f"nu2 = {nu2}")
print(f"eta3 = {eta3}")
print(f"nu3 = {nu3}")


mR0 = np.round(np.mean(destim["R0"]), 4)
qR0 = np.round(np.quantile(destim["R0"], [0.025, 0.975]), 4)

meta = np.round(np.mean(destim["eta"]), 4)
qeta = np.round(np.quantile(destim["eta"], [0.025, 0.975]), 4)


R02 = mR0*(1 - meta)
R02l = qR0[0]*(1 - qeta[1])
R02u = qR0[1]*(1 - qeta[0])

R03 = R02 * (1+eta2)
R03l = R02l * (1+eta2)
R03u = R02u * (1+eta2)

R04 = R03 * (1+eta3)
R04l = R03l * (1+eta3)
R04u = R03u * (1+eta3)

print(f"R01 = {mR0:.1f} (95% CI ({qR0[0]:.1f}–{qR0[1]:.1f})")
print(f"R02 = {R02:.1f} (95% CI ({R02l:.1f}–{R02u:.1f})")
print(f"R03 = {R03:.1f} (95% CI ({R03l:.1f}–{R03u:.1f})")
print(f"R04 = {R04:.1f} (95% CI ({R04l:.1f}–{R04u:.1f})")

print(f"Reduction1 relative to initial R0 {(1- (R02/mR0)):.2f}")
print(f"Reduction2 relative to initial R0{(1 -(R03/mR0)):.2f}")
print(f"Reduction3 relative to initial {(1- (R04/mR0)):.2f}")
