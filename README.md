# About COVIDNC

Data and source code for :
"Evaluating the strategies to control
SARS-CoV-2 Delta variant spread in New Caledonia, a Zero COVID country until September 2021."
# Scripts


- `model.py`: Code for the model's ODE 

- `simICU.py`: Functions for ICU statistical modelling 

- `RO_MCMC.py`: Bayesian parameters estimation for the lockdown period

- `calibration.py`: Subsequent parameters calibration for following periods

- `scenarios.py`: Alternative scenarios simulation

- `ICU.py`: ICU simulation for baseline and alternative scenarios

- `figure.py`: Code for figures

- `extract.py`: Code for Supplementary Figure 1 and extract various 
statistics reported in the manuscript

- `diagram.py`: Code for Supplementary figure 6

# Data

- `dfobs.csv`: Daily hospital and ICU admissions, daily ICU occupancy,
daily maximum ICU capacity, daily new reported cases and 
incidence per 100,000 population.
